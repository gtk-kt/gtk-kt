import org.gtk.dsl.gio.*
import org.gtk.dsl.gtk.*

/**
 * this correlates to the GTK3 packing tutorial
 */
fun main() {
	val result = application("org.gtk.example") {
		onCreateUI {
			menuBar {
				submenu("File") {
					item("Open")
					submenu("Recent") {
						item("A")
						item("B")
						item("C")
					}

					section {
						item("Quit")
					}
				}

				submenu("Edit")
				submenu("View")
				submenu("Help")
			}

			applicationWindow {
				title = "Kotlin/Native Gtk Application-Window test"
				defaultSize = 600 x 200

				addOnActivateDefaultCallback {
					println("Activated default.")
				}
				addOnActivateFocusCallback {
					println("Activated focus.")
				}
				addOnEnableDebuggingCallback {
					println("New debugging state: $it")
				}
				addOnKeysChangedCallback {
					println("Keys changed.")
				}
				addOnSetFocusCallback {
					println("Focusing onto: $it(${it?.name})")
				}

				button("Toggle menu bar") {
					onClicked {
						this@applicationWindow.showMenuBar = !this@applicationWindow.showMenuBar
					}
				}

			}.show()
		}
	}
	println("Final status: $result")
}