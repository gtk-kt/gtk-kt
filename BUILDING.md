Within the working directory

- `gtk`Core library
   ```bash
   ./gradlew -p src/gtk/ publishToMavenLocal
   ```
- `coroutines` Extensions for coroutines
   ```bash
   ./gradlew -p coroutines/ publishToMavenLocal
   ```
- `dsl` DSL bindings to produce UIs easily
   ```bash
   ./gradlew -p dsl/ publishToMavenLocal
   ```
- `ktx` Extension library for convience functions
  ```bash
  ./gradlew -p ktx/ publishToMavenLocal
  ```
- `*` Install all possible files
  ```bash
  ./gradlew publishToMavenLocal
  ```


