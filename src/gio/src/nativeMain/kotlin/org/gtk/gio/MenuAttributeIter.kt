package org.gtk.gio

import gio.*
import glib.GVariant_autoptrVar
import glib.gcharVar
import kotlinx.cinterop.*
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 09 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.MenuAttributeIter.html">
 *     GMenuAttributeIter</a>
 */
class MenuAttributeIter(val menuAttributeIterPointer: GMenuAttributeIter_autoptr) :
	KGObject(menuAttributeIterPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuAttributeIter.get_name.html">
	 *     g_menu_attribute_iter_get_name</a>
	 */
	val name: String
		get() = g_menu_attribute_iter_get_name(menuAttributeIterPointer)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuAttributeIter.get_next.html">
	 *     g_menu_attribute_iter_get_next</a>
	 */
	fun getNext(nextLink: (name: String, value: Variant) -> Unit): Boolean {
		memScoped {
			val outLink = cValue<CPointerVar<gcharVar>>()
			val value = cValue<GVariant_autoptrVar>()

			val result = g_menu_attribute_iter_get_next(
				menuAttributeIterPointer,
				outLink,
				value
			).bool

			if (result) {
				nextLink(
					outLink.ptr.pointed.value!!.toKString(),
					value.ptr.pointed.value!!.wrap()
				)
			}

			return result
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuAttributeIter.get_value.html">
	 *     g_menu_attribute_iter_get_value</a>
	 */
	val value: Variant
		get() = g_menu_attribute_iter_get_value(menuAttributeIterPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuAttributeIter.next.html">
	 *     g_menu_attribute_iter_next</a>
	 */
	fun next(): Boolean =
		g_menu_attribute_iter_next(menuAttributeIterPointer).bool

	companion object {
		inline fun GMenuAttributeIter_autoptr?.wrap() =
			this?.wrap()

		inline fun GMenuAttributeIter_autoptr.wrap() =
			MenuAttributeIter(this)
	}
}