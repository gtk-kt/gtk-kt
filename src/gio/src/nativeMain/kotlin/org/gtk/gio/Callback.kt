package org.gtk.gio

import org.gtk.gobject.KGObject


typealias AsyncReadyCallback = (sourceObject: KGObject?, result: AsyncResult) -> Unit

typealias TypedAsyncReadyCallback<T> = T.(result: AsyncResult) -> Unit