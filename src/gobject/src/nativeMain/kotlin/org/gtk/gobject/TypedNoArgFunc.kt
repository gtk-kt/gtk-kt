package org.gtk.gobject

typealias TypedNoArgFunc<T> = T.() -> Unit