package org.gtk.gobject

typealias TypedNoArgForBooleanFunc<T> = T.() -> Boolean