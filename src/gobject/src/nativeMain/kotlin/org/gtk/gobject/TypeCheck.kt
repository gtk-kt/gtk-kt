package org.gtk.gobject

import gobject.GType
import gobject.g_type_check_instance_cast
import gobject.g_type_check_instance_is_a
import kotlinx.cinterop.CPointed
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool

/**
 * 30 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gobject/func.type_check_instance_cast.html">
 *     g_type_check_instance_cast</a>
 */
fun <T : CPointed> typeCheckInstanceCast(
	instance: TypeInstance,
	gType: GType
): CPointer<T>? =
	g_type_check_instance_cast(instance.typeInstancePointer, gType)?.reinterpret()

/**
 * 30 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gobject/func.type_check_instance_is_a.html">
 *     g_type_check_instance_is_a</a>
 */
fun typeCheckInstanceIsA(
	instance: TypeInstance,
	gType: GType
): Boolean =
	g_type_check_instance_is_a(instance.typeInstancePointer, gType).bool

/**
 * Checks if an [instance] is of [gType], otherwise throws [TypeCastException]
 *
 * @throws TypeCastException
 */
@Throws(TypeCastException::class)
fun <T : CPointed> typeCheckInstanceCastOrThrow(
	instance: TypeInstance,
	gType: GType
): CPointer<T> {

	if (!typeCheckInstanceIsA(instance, gType))
		throw TypeCastException(
			"Couldn't cast " +
					"${instance.gClass.gType.name} " +
					"to ${WrappedType(gType).name}"
		)
	return instance.typeInstancePointer.reinterpret()
}