
import org.gtk.gobject.Value
import org.gtk.gobject.getValue
import org.gtk.gobject.setValue
import kotlin.test.Test

/**
 * 12 / 04 / 2023
 *
 * @see <a href=""></a>
 */
class GValueTest {

	@Test
	fun int() {
		var kValue = 10
		var cValue: Int by Value(10)
		assert(kValue == cValue)

		kValue = 5
		cValue = 5
		assert(kValue == cValue)
	}
}
