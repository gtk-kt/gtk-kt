import org.gtk.glib.StringPtrArray
import kotlin.test.Test

/**
 * 18 / 12 / 2021
 *
 * @see <a href=""></a>
 */


@Test
fun StringPtrArrayTest() {
	val array = StringPtrArray()
	array.add("Hello")

	assert(array.length == 1u) { "StringPtrArray length is incorrect" }
}