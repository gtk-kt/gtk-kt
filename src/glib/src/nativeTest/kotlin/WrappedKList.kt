import kotlinx.cinterop.*
import org.gtk.glib.KList
import org.gtk.glib.WrappedKList
import kotlin.test.Test

/**
 * kotlinx-gtk
 *
 * 21 / 08 / 2021
 *
 * @see <a href=""></a>
 */

@Test
fun WrappedKListStringTest() {
	memScoped {
		val stringList = WrappedKList(
			kList = KList(),
			wrapPointer = { reinterpret<ByteVarOf<Byte>>().toKString() },
			getPointer = { cstr.ptr }
		)

		stringList.append("second")

		assert(stringList.length == 2u) { "Length did not match" }

		val firstIndexData = stringList.getData(1u)

		assert(
			firstIndexData == "second"
		) {
			"Index 1 string does not match expected, Received: $firstIndexData"
		}
	}
}

@Test
fun WrappedStringKListTest() {
	memScoped {
		val stringList = WrappedKList(
			kList = KList(),
			wrapPointer = { reinterpret<ByteVarOf<Byte>>().toKString() },
			{ cstr.getPointer(memScope) }
		)

		stringList.append("second")

		assert(stringList.length == 2u) { "Length did not match" }

		val firstIndexData = stringList.getData(1u)

		assert(
			firstIndexData == "second"
		) {
			"Index 1 string does not match expected, Received: $firstIndexData"
		}
	}
}