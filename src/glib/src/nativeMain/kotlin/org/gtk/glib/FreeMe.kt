package org.gtk.glib

/**
 * gtk-kt
 *
 * 15 / 10 / 2021
 *
 * Many classes in GTK4 must be freed,
 * this interfaces unifies them for freeing extensions
 */
interface FreeMe {
	fun free()
}

inline fun <T : FreeMe, R> T.use(use: (T) -> R): R =
	use(this).also { free() }