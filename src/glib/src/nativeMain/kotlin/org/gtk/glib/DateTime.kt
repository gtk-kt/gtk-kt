package org.gtk.glib

import glib.GDateTime_autoptr

/**
 * kotlinx-gtk
 *
 * 21 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.DateTime.html">
 *     GDateTime</a>
 */
class DateTime(val pointer: GDateTime_autoptr) {

	companion object {

		inline fun GDateTime_autoptr?.wrap() =
			this?.wrap()

		inline fun GDateTime_autoptr.wrap() =
			DateTime(this)

	}
}