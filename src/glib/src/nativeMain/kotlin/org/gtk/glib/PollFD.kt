package org.gtk.glib

import glib.GPollFD
import kotlinx.cinterop.*

/**
 * kotlinx-gtk
 *
 * 05 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.PollFD.html"></a>
 */
class PollFD(val pollFDPointer: CPointer<GPollFD>) {

	constructor() : this(nativeHeap.alloc<GPollFD>().ptr)

	constructor(nativePlacement: NativePlacement) : this(with(nativePlacement) { alloc<GPollFD>().ptr })
}