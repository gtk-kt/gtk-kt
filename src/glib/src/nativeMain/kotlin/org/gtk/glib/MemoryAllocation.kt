package org.gtk.glib

import glib.g_free
import glib.g_strfreev
import glib.gpointer

/**
 * @see <a href="https://docs.gtk.org/glib/func.free.html">
 *     g_free</a>
 */
inline fun gpointer.free() = g_free(this)

/**
 * @see <a href="https://docs.gtk.org/glib/func.strfreev.html">
 *     g_strfreev</a>
 */
inline fun CStringList.free() = g_strfreev(this)
