package org.gtk.glib

import kotlinx.cinterop.*

/*
 * kotlinx-gtk
 *
 * 20 / 08 / 2021
 */

/**
 * Kotlin typealias for a C Array
 */
typealias CArray<T> = CPointer<CPointerVar<T>>

/**
 * Turn a NULL terminated C Array into a Kotlin Array
 *
 * `set` operations on the returned array does not modify the original C Array.
 */
inline fun <reified T : CPointed> CArray<T>.toArray(): Array<CPointer<T>> {
	var size = 0

	// get max size
	run {
		while (get(size) != null) {
			size++
		}
	}

	return Array(size) { get(it)!! }
}

inline fun CPointer<IntVar>.toArray(): Array<Int> {
	var size = 0

	// get max size
	run {
		while ((this + size) != null) {
			size++
		}
	}

	return Array(size) { get(it) }
}

inline fun CPointer<IntVar>.freeToArray(): Array<Int> {
	var size = 0

	// get max size
	run {
		while ((this + size) != null) {
			size++
		}
	}

	return Array(size) { get(it) }.also { free() }
}


/**
 * Turn a C Array into a Kotlin Array with a known size
 *
 * `set` operations on the returned Array does not modify the original C Array.
 */
inline fun <reified T : CPointed> CArray<T>.toArray(size: Int): Array<CPointer<T>> =
	Array(size) { get(it)!! }

/**
 * Turn a NULL terminated C Array into a Kotlin List, with action to wrap each index
 *
 * `set` operations on the returned array does not modify the original C Array.
 */
inline fun <reified T : CPointed, O> CArray<T>.toWrappedList(wrap: (CPointer<T>) -> O): List<O> =
	toArray().map(wrap)

/**
 * Turn a C Array into a Kotlin List with a known size, with action to wrap each index
 *
 * `set` operations on the returned array does not modify the original C Array.
 */
inline fun <reified T : CPointed, O> CArray<T>.toWrappedList(
	size: Int,
	wrap: (CPointer<T>) -> O,
): List<O> =
	toArray(size).map(wrap)


inline fun CArray<UIntVar>.toList(): List<UInt> =
	toWrappedList { it.pointed.value }

inline fun CArray<UIntVar>.toList(size: Int): List<UInt> =
	toWrappedList(size) { it.pointed.value }

inline fun CStringList.toList(): List<String> =
	toWrappedList { it.toKString() }

fun Array<Int>.toCPointerArray(scope: MemScope): CPointer<CPointerVar<IntVar>> =
	with(scope) {
		allocArrayOf(this@toCPointerArray.map { value ->
			cValue<IntVar>().apply {
				this.ptr.pointed.value = value
			}.getPointer(this)
		})
	}

fun Array<ULong>.toCPointerArray(
	scope: MemScope,
): CPointer<CPointerVar<ULongVar>> =
	with(scope) {
		val allocArrayOf = allocArrayOf(this@toCPointerArray.map { value ->
			cValue<ULongVar>().apply {
				this.ptr.pointed.value = value
			}.getPointer(this)
		})
		allocArrayOf
	}


fun Array<ULong>.toCArray(scope: MemScope): CPointer<ULongVar> =
	with(scope) {
		allocArray(this@toCArray.size) { index ->
			value = this@toCArray[index]
		}
	}

fun Array<Int>.toCArray(scope: MemScope): CPointer<IntVar> =
	with(scope) {
		allocArray(this@toCArray.size) { index ->
			value = this@toCArray[index]
		}
	}

fun Array<String>.toNullTermCStringArray(): CStringList =
	memScoped {
		allocArrayOf(this@toNullTermCStringArray.map { it.cstr.getPointer(this) } + null)
	}

fun Array<out String>.toNullTermCStringArray(): CStringList =
	memScoped {
		allocArrayOf(this@toNullTermCStringArray.map { it.cstr.getPointer(this) } + null)
	}

fun <T, O : CPointed> Array<T>.toNullTermCArray(
	map: T.() -> CPointer<O>,
): CPointer<CPointerVarOf<CPointer<O>>> = memScoped {
	allocArrayOf(this@toNullTermCArray.map { it.map() } + null)
}


inline fun CStringList.toArray(): Array<String> {
	var size = 0

	// get max size
	run {
		while (get(size) != null) {
			size++
		}
	}

	return Array(size) { get(it)!!.toKString() }
}

/**
 * Like the toArray version, except frees the pointer
 */
inline fun CStringList.freeToArray(): Array<String> {
	var size = 0

	// get max size
	run {
		while (get(size) != null) {
			size++
		}
	}

	return Array(size) { get(it)!!.toKString() }.also { free() }
}