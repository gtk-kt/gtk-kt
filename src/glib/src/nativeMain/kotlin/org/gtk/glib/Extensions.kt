package org.gtk.glib

import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointed
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.StableRef


/**
 * Use a pointer, then the pointer will be freed afterwards
 *
 * @see [CPointer.free]
 * @return return the result from [action]
 */
inline fun <I : CPointed, O> CPointer<I>.use(action: (CPointer<I>) -> O): O =
	let(action).also { this.free() }

/**
 * Use a StableRef, disposing of it afterwards
 *
 * @see [StableRef.dispose]
 * @return return result from [action]
 */
inline fun <I : Any, O> StableRef<I>.use(action: (StableRef<I>) -> O): O =
	let(action).also { dispose() }

/**
 * Use a StableRef (as a CPointer), disposing of it afterwards
 *
 * @see [StableRef.asCPointer]
 * @see [StableRef.dispose]
 * @return return result from [action]
 */
inline fun <I : Any, O> StableRef<I>.usePointer(action: (COpaquePointer) -> O): O =
	action(asCPointer()).also { dispose() }

/**
 * Turns anything into a stable ref pointer
 */
inline fun Any.asStablePointer(): COpaquePointer =
	StableRef.create(this).asCPointer()