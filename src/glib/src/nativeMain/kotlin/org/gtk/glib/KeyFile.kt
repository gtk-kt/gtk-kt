package org.gtk.glib

import glib.GKeyFile
import glib.g_key_file_error_quark
import glib.g_key_file_new
import kotlinx.cinterop.CPointer

/**
 * 05 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/glib/type_func.KeyFile.error_quark.html">
 *     GKeyFile</a>
 */
class KeyFile(
	val structPointer: CPointer<GKeyFile>,
) {

	constructor() : this(g_key_file_new()!!)

	companion object {
		fun quark(): UInt =
			g_key_file_error_quark()
	}
}