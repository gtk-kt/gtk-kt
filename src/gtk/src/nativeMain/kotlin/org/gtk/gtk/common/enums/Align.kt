package org.gtk.gtk.common.enums;

import gtk.GtkAlign
import gtk.GtkAlign.*

/**
 * @see <a href="https://docs.gtk.org/gtk4/enum.Align.html">GtkAlign</a>
 */
enum class Align(val gtk: GtkAlign) {
	FILL(GTK_ALIGN_FILL),
	START(GTK_ALIGN_START),
	END(GTK_ALIGN_END),
	CENTER(GTK_ALIGN_CENTER),
	BASELINE(GTK_ALIGN_BASELINE);

	companion object {
		fun valueOf(gtk: GtkAlign) =
			when (gtk) {
				GTK_ALIGN_FILL -> FILL
				GTK_ALIGN_START -> START
				GTK_ALIGN_END -> END
				GTK_ALIGN_CENTER -> CENTER
				GTK_ALIGN_BASELINE -> BASELINE
			}
	}
}