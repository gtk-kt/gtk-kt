package org.gtk.gtk.widgets.button

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Actionable
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 16 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.LinkButton.html">
 *     GtkLinkButton</a>
 */
class LinkButton(
	val linkButtonPointer: GtkLinkButton_autoptr,
) : Button(linkButtonPointer.reinterpret()), Actionable {

	override val actionablePointer: GtkActionable_autoptr =
		linkButtonPointer.reinterpret()

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_LINK_BUTTON))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.LinkButton.new.html">
	 *     gtk_link_button_new</a>
	 */
	constructor(uri: String) : this(gtk_link_button_new(uri)!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.LinkButton.new_with_label.html">
	 *     gtk_link_button_new_with_label</a>
	 */
	constructor(uri: String, label: String) : this(
		gtk_link_button_new_with_label(uri, label)!!.reinterpret()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.LinkButton.get_uri.html">
	 *     gtk_link_button_get_uri</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.LinkButton.set_uri.html">
	 *     gtk_link_button_set_uri</a>
	 */
	var uri: String
		get() = gtk_link_button_get_uri(linkButtonPointer)!!.toKString()
		set(value) = gtk_link_button_set_uri(linkButtonPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.LinkButton.get_visited.html">
	 *     gtk_link_button_get_visited</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.LinkButton.set_visited.html">
	 *     gtk_link_button_set_visited</a>
	 */
	var visited: Boolean
		get() = gtk_link_button_get_visited(linkButtonPointer).bool
		set(value) = gtk_link_button_set_visited(
			linkButtonPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.LinkButton.activate-link.html">
	 *     activate-link</a>
	 */
	fun addOnActivateLinkCallback(action: TypedNoArgFunc<LinkButton>): SignalManager =
		addSignalCallback(Signals.ACTIVATE_LINK, action, staticNoArgGCallback)

	companion object {
		inline fun GtkLinkButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkLinkButton_autoptr.wrap() =
			LinkButton(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkLinkButton_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<LinkButton>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}