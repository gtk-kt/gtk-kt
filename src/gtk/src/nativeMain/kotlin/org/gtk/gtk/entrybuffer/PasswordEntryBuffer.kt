package org.gtk.gtk.entrybuffer

import gtk.GtkPasswordEntryBuffer_autoptr
import gtk.gtk_password_entry_buffer_new
import kotlinx.cinterop.reinterpret

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PasswordEntryBuffer.html">
 *     GtkPasswordEntryBuffer</a>
 */
class PasswordEntryBuffer(
	val passwordEntryPointer: GtkPasswordEntryBuffer_autoptr,
) : EntryBuffer(passwordEntryPointer.reinterpret()) {

	constructor() : this(gtk_password_entry_buffer_new()!!.reinterpret())

}