package org.gtk.gtk.filter

import gtk.*
import gtk.GtkStringFilterMatchMode.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.StringFilter.html">
 *     GtkStringFilter</a>
 */
class StringFilter(val stringFilterPointer: GtkStringFilter_autoptr) :
	Filter(stringFilterPointer.reinterpret()) {

	constructor(expression: Expression?) :
			this(gtk_string_filter_new(expression?.expressionPointer)!!)

	var expression: Expression?
		get() = gtk_string_filter_get_expression(stringFilterPointer).wrap()
		set(value) = gtk_string_filter_set_expression(stringFilterPointer, value?.expressionPointer)

	var ignoreCase: Boolean
		get() = gtk_string_filter_get_ignore_case(stringFilterPointer).bool
		set(value) = gtk_string_filter_set_ignore_case(stringFilterPointer, value.gtk)

	var matchMode: MatchMode
		get() = MatchMode.valueOf(gtk_string_filter_get_match_mode(stringFilterPointer))
		set(value) = gtk_string_filter_set_match_mode(stringFilterPointer, value.gtk)

	var search: String?
		get() = gtk_string_filter_get_search(stringFilterPointer)?.toKString()
		set(value) = gtk_string_filter_set_search(stringFilterPointer, value)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.StringFilterMatchMode.html">
	 *     GtkStringFilterMatchMode</a>
	 */
	enum class MatchMode(val gtk: GtkStringFilterMatchMode) {
		EXACT(GTK_STRING_FILTER_MATCH_MODE_EXACT),
		SUBSTRING(GTK_STRING_FILTER_MATCH_MODE_SUBSTRING),
		PREFIX(GTK_STRING_FILTER_MATCH_MODE_PREFIX);

		companion object {
			fun valueOf(gtk: GtkStringFilterMatchMode) =
				when (gtk) {
					GTK_STRING_FILTER_MATCH_MODE_EXACT -> EXACT
					GTK_STRING_FILTER_MATCH_MODE_SUBSTRING -> SUBSTRING
					GTK_STRING_FILTER_MATCH_MODE_PREFIX -> PREFIX
				}
		}
	}
}