package org.gtk.gtk.widgets

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.common.enums.ScrollType

/**
 * kotlinx-gtk
 * 24 / 03 / 2021
 * @see <a href="https://docs.gtk.org/gtk4/class.Paned.html">
 *     GtkPaned</a>
 */
open class Paned(
	val panedPointer: GtkPaned_autoptr,
) : Widget(panedPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_PANED))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Paned.new.html">
	 *     gtk_paned_new</a>
	 */
	constructor(orientation: Orientation) :
			this(gtk_paned_new(orientation.gtk)!!.reinterpret())

	var endChild: Widget?
		get() = gtk_paned_get_end_child(panedPointer).wrap()
		set(value) = gtk_paned_set_end_child(panedPointer, value?.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Paned.get_position.html">
	 *     gtk_paned_get_position</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Paned.set_position.html">
	 *     gtk_paned_set_position</a>
	 */
	var position: Int
		get() = gtk_paned_get_position(panedPointer)
		set(value) = gtk_paned_set_position(panedPointer, value)

	var resizeEndChild: Boolean
		get() = gtk_paned_get_resize_end_child(panedPointer).bool
		set(value) = gtk_paned_set_resize_end_child(panedPointer, value.gtk)

	var resizeStartChild: Boolean
		get() = gtk_paned_get_resize_start_child(panedPointer).bool
		set(value) = gtk_paned_set_resize_start_child(panedPointer, value.gtk)

	var shrinkEndChild: Boolean
		get() = gtk_paned_get_shrink_end_child(panedPointer).bool
		set(value) = gtk_paned_set_shrink_end_child(panedPointer, value.gtk)

	var shrinkStartChild: Boolean
		get() = gtk_paned_get_shrink_start_child(panedPointer).bool
		set(value) = gtk_paned_set_shrink_start_child(panedPointer, value.gtk)

	var startChild: Widget?
		get() = gtk_paned_get_start_child(panedPointer).wrap()
		set(value) = gtk_paned_set_start_child(panedPointer, value?.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Paned.get_wide_handle.html">
	 *     gtk_paned_get_wide_handle</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Paned.set_wide_handle.html">
	 *     gtk_paned_set_wide_handle</a>
	 */
	var wideHandle: Boolean
		get() = gtk_paned_get_wide_handle(panedPointer).bool
		set(value) = gtk_paned_set_wide_handle(panedPointer, value.gtk)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.accept-position.html">
	 *     accept-position</a>
	 */
	fun setAcceptPositionCallback(action: PanedNoArgForBooleanFunc) =
		addSignalCallback(Signals.ACCEPT_POSITION, action, staticNoArgBooleanCallback)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.cancel-position.html">
	 *     cancel-position</a>
	 */
	fun setCancelPositionCallback(action: PanedNoArgForBooleanFunc) =
		addSignalCallback(Signals.CANCEL_POSITION, action, staticNoArgBooleanCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.cycle-child-focus.html">
	 *     cycle-child-focus</a>
	 */
	fun setCycleChildFocusCallback(action: PanedCycleFunc) =
		addSignalCallback(Signals.CYCLE_CHILD_FOCUS,action, staticCycleCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.cycle-handle-focus.html">
	 *     cycle-handle-focus</a>
	 */
	fun setCycleHandleFocusCallback(action: PanedCycleFunc) =
		addSignalCallback(Signals.CYCLE_HANDLE_FOCUS,action, staticCycleCallback)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.move-handle.html">
	 *     move-handle</a>
	 */
	fun setMoveHandleCallback(action: PanedMoveHandleFunc) =
		addSignalCallback(Signals.MOVE_HANDLE,action, staticMoveHandleCallback)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Paned.toggle-handle-focus.html">
	 *     toggle-handle-focus</a>
	 */
	fun setToggleHandleFocusCallback(action: PanedNoArgForBooleanFunc) =
		addSignalCallback(Signals.TOGGLE_HANDLE_FOCUS, action, staticNoArgBooleanCallback)

	companion object {

		inline fun GtkPaned_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkPaned_autoptr.wrap() =
			Paned(this)

		private val staticMoveHandleCallback: GCallback =
			staticCFunction {
					self: GtkPaned_autoptr,
					scrollType: GtkScrollType,
					data: gpointer,
				->
				data.asStableRef<PanedMoveHandleFunc>()
					.get()
					.invoke(self.wrap(), ScrollType.valueOf(scrollType)!!).gtk
			}.reinterpret()

		private val staticNoArgBooleanCallback: GCallback =
			staticCFunction {
					self: GtkPaned_autoptr,
					data: gpointer,
				->
				data.asStableRef<PanedNoArgForBooleanFunc>()
					.get()
					.invoke(self.wrap()).gtk
			}.reinterpret()

		private val staticCycleCallback: GCallback =
			staticCFunction {
					self: GtkPaned_autoptr,
					reversed: gboolean,
					data: gpointer,
				->
				data.asStableRef<PanedCycleFunc>()
					.get()
					.invoke(self.wrap(), reversed.bool).gtk
			}.reinterpret()
	}
}

typealias PanedNoArgForBooleanFunc = Paned.() -> Boolean
typealias PanedCycleFunc = Paned.(reversed: Boolean) -> Boolean
typealias PanedMoveHandleFunc = Paned.(ScrollType) -> Boolean