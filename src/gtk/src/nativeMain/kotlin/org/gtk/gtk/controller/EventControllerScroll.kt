package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EventControllerScroll.html">
 *     GtkEventControllerScroll</a>
 */
class EventControllerScroll(
	val motionPointer: CPointer<GtkEventControllerScroll>,
) : EventController(motionPointer.reinterpret()) {

	constructor() : this(gtk_event_controller_motion_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_EVENT_CONTROLLER_SCROLL
	))

	var flags: GtkEventControllerScrollFlags
		get() = gtk_event_controller_scroll_get_flags(motionPointer)
		set(value) = gtk_event_controller_scroll_set_flags(motionPointer, value)

	fun addOnDecelerateCallback(action: EventControllerScrollVelXYFunc) =
		addSignalCallback(Signals.DECELERATE, action, staticXYGCallback)

	fun addOnScrollCallback(action: EventControllerScrollFunc) =
		addSignalCallback(Signals.SCROLL, action, staticXYGCallback)

	fun addOnScrollBeginCallback(action: TypedNoArgFunc<EventControllerScroll>) =
		addSignalCallback(Signals.SCROLL_BEGIN, action, staticNoArgGCallback)

	fun addOnScrollEndCallback(action: TypedNoArgFunc<EventControllerScroll>) =
		addSignalCallback(Signals.SCROLL_END, action, staticNoArgGCallback)


	companion object {
		inline fun CPointer<GtkEventControllerScroll>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEventControllerScroll>.wrap() =
			EventControllerScroll(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerScroll>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<EventControllerScroll>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticXYGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerScroll>,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<EventControllerScroll.(
					Double,
					Double,
				) -> Unit>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()
	}
}

typealias EventControllerScrollVelXYFunc =
		EventControllerScroll.(
			velX: Double,
			velY: Double,
		) -> Unit

typealias EventControllerScrollFunc =
		EventControllerScroll.(
			dx: Double,
			dy: Double,
		) -> Boolean