package org.gtk.gtk.cellrenderer

import glib.gpointer
import glib.guint
import gobject.GCallback
import gtk.*
import gtk.GtkCellRendererAccelMode.GTK_CELL_RENDERER_ACCEL_MODE_GTK
import gtk.GtkCellRendererAccelMode.GTK_CELL_RENDERER_ACCEL_MODE_OTHER
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererAccel.html">
 *     GtkCellRendererAccel</a>
 */
class CellRendererAccel(
	val accelPointer: GtkCellRendererAccel_autoptr,
) : CellRendererText(accelPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_accel_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_ACCEL
	))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.CellRendererAccelMode.html">
	 *     GtkCellRendererAccelMode</a>
	 */
	enum class Mode(val gtk: GtkCellRendererAccelMode) {
		GTK(GTK_CELL_RENDERER_ACCEL_MODE_GTK),
		OTHER(GTK_CELL_RENDERER_ACCEL_MODE_OTHER);

		companion object {
			fun valueOf(gtk: GtkCellRendererAccelMode) =
				when (gtk) {
					GTK_CELL_RENDERER_ACCEL_MODE_GTK -> GTK
					GTK_CELL_RENDERER_ACCEL_MODE_OTHER -> OTHER
				}
		}
	}

	fun addOnAccelClearedCallback(action: CellRendererAccelClearedFunc) =
		addSignalCallback(Signals.ACCEL_CLEARED, action, staticAccelClearedCallback)

	fun addOnAccelEditedCallback(action: CellRendererAccelEditedFunc) =
		addSignalCallback(Signals.ACCEL_EDITED, action, staticAccelEditedCallback)


	companion object {
		inline fun GtkCellRendererAccel_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellRendererAccel_autoptr.wrap() =
			CellRendererAccel(this)

		private val staticAccelClearedCallback: GCallback =
			staticCFunction {
					self: GtkCellRendererAccel_autoptr,
					path_string: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellRendererAccelClearedFunc>()
					.get()
					.invoke(self.wrap(), path_string.toKString())
			}.reinterpret()

		private val staticAccelEditedCallback: GCallback =
			staticCFunction {
					self: GtkCellRendererAccel_autoptr,
					path_string: CStringPointer,
					accel_key: guint,
					accel_mods: GdkModifierType,
					hardware_keycode: guint,
					data: gpointer,
				->
				data.asStableRef<CellRendererAccelEditedFunc>()
					.get()
					.invoke(
						self.wrap(),
						path_string.toKString(),
						accel_key,
						accel_mods,
						hardware_keycode
					)
			}.reinterpret()
	}
}

typealias CellRendererAccelClearedFunc =
		CellRendererAccel.(
			pathString: String,
		) -> Unit


typealias CellRendererAccelEditedFunc =
		CellRendererAccel.(
			pathString: String,
			accelKey: UInt,
			accelMods: GdkModifierType,
			hardwareKeycode: UInt,
		) -> Unit