package org.gtk.gtk.common.enums

import gtk.GtkEventSequenceState
import gtk.GtkEventSequenceState.*

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.EventSequenceState.html">
 *     GtkEventSequenceState</a>
 */
enum class EventSequenceState(
	val gtk: GtkEventSequenceState,
) {
	NONE(GTK_EVENT_SEQUENCE_NONE),
	CLAIMED(GTK_EVENT_SEQUENCE_CLAIMED),
	DENIED(GTK_EVENT_SEQUENCE_DENIED);

	companion object {
		fun valueOf(gtk: GtkEventSequenceState) =
			when (gtk) {
				GTK_EVENT_SEQUENCE_NONE -> NONE
				GTK_EVENT_SEQUENCE_CLAIMED -> CLAIMED
				GTK_EVENT_SEQUENCE_DENIED -> DENIED
			}
	}
}