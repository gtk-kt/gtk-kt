package org.gtk.gtk.widgets.box

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Accessible
import org.gtk.gtk.Buildable
import org.gtk.gtk.ConstraintTarget
import org.gtk.gtk.Orientable
import org.gtk.gtk.common.enums.BaselinePosition
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 07 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Box.html">GtkBox</a>
 */
open class Box(
	val boxPointer: CPointer<GtkBox>
) : Widget(boxPointer.reinterpret()),
	Orientable {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_BOX))

	override val orientablePointer: CPointer<GtkOrientable> by lazy { boxPointer.reinterpret() }

	constructor(
		orientation: Orientation,
		spacing: Int
	) : this(gtk_box_new(orientation.gtk, spacing)!!.reinterpret())

	fun append(widget: Widget) {
		gtk_box_append(boxPointer, widget.widgetPointer)
	}

	var baselinePosition: BaselinePosition
		get() = BaselinePosition.valueOf(
			gtk_box_get_baseline_position(
				boxPointer
			)
		)
		set(value) = gtk_box_set_baseline_position(boxPointer, value.gtk)

	var isHomogeneous: Boolean
		get() = gtk_box_get_homogeneous(boxPointer).bool
		set(value) = gtk_box_set_homogeneous(boxPointer, value.gtk)

	var spacing: Int
		get() = gtk_box_get_spacing(boxPointer)
		set(value) = gtk_box_set_spacing(boxPointer, value)

	fun insertChildAfter(child: Widget, sibling: Widget) {
		gtk_box_insert_child_after(boxPointer, child.widgetPointer, sibling.widgetPointer)
	}

	fun prepend(widget: Widget) {
		gtk_box_prepend(boxPointer, widget.widgetPointer)
	}

	fun remove(widget: Widget) {
		gtk_box_remove(boxPointer, widget.widgetPointer)
	}

	fun reorderChildAfter(child: Widget, sibling: Widget) {
		gtk_box_reorder_child_after(boxPointer, child.widgetPointer, sibling.widgetPointer)
	}

	companion object {
		inline fun CPointer<GtkBox>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkBox>.wrap() =
			Box(this)
	}
}