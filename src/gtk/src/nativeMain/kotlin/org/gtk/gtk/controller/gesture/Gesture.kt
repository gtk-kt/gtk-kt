package org.gtk.gtk.controller.gesture

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Device
import org.gtk.gdk.Device.Companion.wrap
import org.gtk.gdk.Event
import org.gtk.gdk.Event.Companion.wrap
import org.gtk.gdk.EventSequence
import org.gtk.gdk.EventSequence.Companion.wrap
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.WrappedKList
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.common.data.PreciseCoordinates
import org.gtk.gtk.common.enums.EventSequenceState
import org.gtk.gtk.controller.EventController

/**
 * gtk-kt
 *
 * 26 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Gesture.html">GtkGesture</a>
 */
open class Gesture(
	val gesturePointer: GtkGesture_autoptr,
) : EventController(gesturePointer.reinterpret()) {

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE))

	val boundingBox: Rectangle?
		get() = memScoped {
			val rect = alloc<GdkRectangle>()

			val r = gtk_gesture_get_bounding_box(
				gesturePointer,
				rect.ptr
			)

			if (r.bool) rect.ptr.wrap() else null
		}

	val boundingBoxCenter: PreciseCoordinates?
		get() = memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()

			val r = gtk_gesture_get_bounding_box_center(
				gesturePointer,
				x,
				y
			)

			if (r.bool) {
				PreciseCoordinates(x.ptr.pointed.value, y.ptr.pointed.value)
			} else {
				null
			}
		}

	val device: Device?
		get() = gtk_gesture_get_device(gesturePointer).wrap()

	val group: WrappedKList<Gesture>
		get() = gtk_gesture_get_group(gesturePointer)!!.asWrappedKList(
			{ reinterpret<GtkGesture>().wrap() },
			{ gesturePointer }
		)

	fun getLastEvent(sequence: EventSequence?): Event? =
		gtk_gesture_get_last_event(
			gesturePointer,
			sequence?.eventSequencePointer
		).wrap()

	val lastUpdateSequence: EventSequence?
		get() = gtk_gesture_get_last_updated_sequence(gesturePointer).wrap()

	fun getPoint(sequence: EventSequence?): PreciseCoordinates? =
		memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()

			val r = gtk_gesture_get_point(
				gesturePointer,
				sequence?.eventSequencePointer,
				x,
				y
			)

			if (r.bool) {
				PreciseCoordinates(x.ptr.pointed.value, y.ptr.pointed.value)
			} else {
				null
			}
		}

	fun getSequenceState(sequence: EventSequence): EventSequenceState =
		EventSequenceState.valueOf(
			gtk_gesture_get_sequence_state(
				gesturePointer,
				sequence.eventSequencePointer
			)
		)

	val sequences: WrappedKList<EventSequence>
		get() = gtk_gesture_get_sequences(gesturePointer)!!
			.asWrappedKList(
				{ reinterpret<GdkEventSequence>().wrap() },
				{ eventSequencePointer }
			)

	fun group(gesture: Gesture) {
		gtk_gesture_group(
			gesturePointer,
			gesture.gesturePointer
		)
	}

	fun handlesSequence(sequence: EventSequence?): Boolean =
		gtk_gesture_handles_sequence(
			gesturePointer,
			sequence?.eventSequencePointer
		).bool

	val isActive: Boolean
		get() = gtk_gesture_is_active(gesturePointer).bool

	fun isGroupedWith(other: Gesture): Boolean =
		gtk_gesture_is_grouped_with(
			gesturePointer,
			other.gesturePointer
		).bool

	val isRecognized: Boolean
		get() = gtk_gesture_is_recognized(gesturePointer).bool

	fun setSequenceState(
		sequence: EventSequence,
		state: EventSequenceState,
	): Boolean =
		gtk_gesture_set_sequence_state(
			gesturePointer,
			sequence.eventSequencePointer,
			state.gtk
		).bool

	fun setState(state: EventSequenceState): Boolean =
		gtk_gesture_set_state(gesturePointer, state.gtk).bool

	fun ungroup() {
		gtk_gesture_ungroup(gesturePointer)
	}

	fun addOnBeginCallback(action: GestureSequenceFunc) =
		addSignalCallback(Signals.BEGIN, action, staticSequenceFunc)

	fun addOnCancelCallback(action: GestureSequenceFunc)=
		addSignalCallback(Signals.CANCEL, action, staticSequenceFunc)

	fun addOnEndCallback(action: GestureSequenceFunc)=
		addSignalCallback(Signals.END, action, staticSequenceFunc)

	fun addOnSequenceStateChangedCallback(
		action: GestureSequenceStateChangedFunc,
	) = addSignalCallback(
		Signals.SEQUENCE_STATE_CHANGED,
		action,
		staticSequenceStateChangedFunc
	)

	fun addOnUpdateCallback(action: GestureSequenceFunc)=
		addSignalCallback(Signals.UPDATE, action, staticSequenceFunc)

	companion object {
		inline fun GtkGesture_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGesture_autoptr.wrap() =
			Gesture(this)

		private val staticSequenceFunc: GCallback =
			staticCFunction {
					self: GtkGesture_autoptr,
					sequence: CPointer<GdkEventSequence>?,
					data: gpointer,
				->
				data.asStableRef<GestureSequenceFunc>()
					.get()
					.invoke(
						self.wrap(),
						sequence.wrap()
					)
			}.reinterpret()

		private val staticSequenceStateChangedFunc: GCallback =
			staticCFunction {
					self: GtkGesture_autoptr,
					sequence: CPointer<GdkEventSequence>?,
					state: GtkEventSequenceState,
					data: gpointer,
				->
				data.asStableRef<GestureSequenceStateChangedFunc>()
					.get()
					.invoke(
						self.wrap(),
						sequence.wrap(),
						EventSequenceState.valueOf(state)
					)
			}.reinterpret()
	}
}

typealias GestureSequenceFunc =
		Gesture.(
			sequence: EventSequence?,
		) -> Unit

typealias GestureSequenceStateChangedFunc =
		Gesture.(
			sequence: EventSequence?,
			state: EventSequenceState,
		) -> Unit