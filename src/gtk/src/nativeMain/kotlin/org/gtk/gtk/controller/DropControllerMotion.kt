package org.gtk.gtk.controller

import glib.gdouble
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.Drop
import org.gtk.gdk.Drop.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.Signals.ENTER
import org.gtk.gobject.Signals.LEAVE
import org.gtk.gobject.Signals.MOTION
import org.gtk.gobject.addSignalCallback

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DropControllerMotion.html">
 *     GtkDropControllerMotion</a>
 */
class DropControllerMotion(
	val contPointer: CPointer<GtkDropControllerMotion>,
) : EventController(contPointer.reinterpret()) {

	constructor() : this(gtk_drop_controller_motion_new()!!.reinterpret())

	val containsPointer: Boolean
		get() = gtk_drop_controller_motion_contains_pointer(contPointer).bool

	val drop: Drop
		get() = gtk_drop_controller_motion_get_drop(contPointer)!!.wrap()

	val isPointer: Boolean
		get() = gtk_drop_controller_motion_is_pointer(contPointer).bool

	fun addOnEnterCallback(action: XYFunction) =
		addSignalCallback(ENTER, action, staticXYCallback)

	fun addOnLeaveCallback(action: LeaveFunction) =
		addSignalCallback(LEAVE, action, staticLeaveFunction)


	fun addOnMotionCallback(action: XYFunction) =
		addSignalCallback(MOTION, action, staticXYCallback)


	companion object {
		private val staticXYCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkDropControllerMotion>,
					x: gdouble,
					y: gdouble,
					data: gpointer,
				->
				data.asStableRef<XYFunction>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()

		private val staticLeaveFunction: GCallback =
			staticCFunction {
					self: CPointer<GtkDropControllerMotion>,
					data: gpointer,
				->
				data.asStableRef<LeaveFunction>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		inline fun CPointer<GtkDropControllerMotion>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkDropControllerMotion>.wrap() =
			DropControllerMotion(this)
	}
}

typealias XYFunction = DropControllerMotion.(x: Double, y: Double) -> Unit
typealias LeaveFunction = DropControllerMotion.() -> Unit