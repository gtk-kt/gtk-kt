package org.gtk.gtk.trigger

import gtk.GTK_TYPE_NEVER_TRIGGER
import gtk.GtkNeverTrigger_autoptr
import gtk.gtk_never_trigger_get
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NeverTrigger.html">
 *     GtkNeverTrigger</a>
 */
class NeverTrigger(
	val neverPointer: GtkNeverTrigger_autoptr,
) : ShortcutTrigger(neverPointer.reinterpret()) {

	constructor() : this(gtk_never_trigger_get()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_NEVER_TRIGGER))
}