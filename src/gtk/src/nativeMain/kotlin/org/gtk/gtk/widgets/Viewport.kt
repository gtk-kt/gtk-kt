package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Adjustment
import org.gtk.gtk.Scrollable

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkViewport.html">GtkViewport</a>
 */
class Viewport(
	val viewPortPointer: CPointer<GtkViewport>,
) : Widget(viewPortPointer.reinterpret()), Scrollable {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_VIEWPORT))

	override val scrollablePointer: CPointer<GtkScrollable>
		get() = viewPortPointer.reinterpret()

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkViewport.html#gtk-viewport-new">gtk_viewport_new</a>
	 */
	constructor(
		horizontalAdjustment: Adjustment,
		verticalAdjustment: Adjustment,
	) : this(
		gtk_viewport_new(
			horizontalAdjustment.adjustmentPointer,
			verticalAdjustment.adjustmentPointer
		)!!.reinterpret()
	)

	var child: Widget?
		get() = gtk_viewport_get_child(viewPortPointer).wrap()
		set(value) = gtk_viewport_set_child(viewPortPointer, value?.widgetPointer)

	var scrollToFocus: Boolean
		get() = gtk_viewport_get_scroll_to_focus(viewPortPointer).bool
		set(value) = gtk_viewport_set_scroll_to_focus(viewPortPointer, value.gtk)
}