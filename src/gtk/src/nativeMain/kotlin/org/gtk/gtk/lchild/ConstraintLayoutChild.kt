package org.gtk.gtk.lchild

import gtk.GTK_TYPE_CONSTRAINT_LAYOUT_CHILD
import gtk.GtkConstraintLayoutChild_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ConstraintLayoutChild.html">
 *     GtkConstraintLayoutChild</a>
 */
class ConstraintLayoutChild(
	val constraintPointer: GtkConstraintLayoutChild_autoptr,
) : LayoutChild(constraintPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_CONSTRAINT_LAYOUT_CHILD
	))
}