package org.gtk.gtk

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.filter.Filter
import org.gtk.gtk.filter.Filter.Companion.wrap

/**
 * 16 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FilterListModel.html">
 *     GtkFilterListModel</a>
 */
class FilterListModel(
	val filterPointer: GtkFilterListModel_autoptr,
) : KGObject(filterPointer.reinterpret()), ListModel {

	override val listModelPointer: GListModel_autoptr by lazy {
		filterPointer.reinterpret()
	}

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_FILTER_LIST_MODEL
	))

	constructor(
		model: ListModel?,
		filter: Filter?,
	) : this(
		gtk_filter_list_model_new(
			model?.listModelPointer,
			filter?.filterPointer
		)!!
	)

	var filter: Filter?
		get() = gtk_filter_list_model_get_filter(filterPointer).wrap()
		set(value) = gtk_filter_list_model_set_filter(
			filterPointer,
			value?.filterPointer
		)

	var incremental: Boolean
		get() = gtk_filter_list_model_get_incremental(filterPointer).bool
		set(value) = gtk_filter_list_model_set_incremental(
			filterPointer,
			value.gtk
		)

	var model: ListModel?
		get() = gtk_filter_list_model_get_model(filterPointer).wrap()
		set(value) = gtk_filter_list_model_set_model(
			filterPointer,
			value?.listModelPointer
		)

	val pending: UInt
		get() = gtk_filter_list_model_get_pending(filterPointer)
}