package org.gtk.gtk.widgets

import gtk.*
import gtk.GtkStackTransitionType.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.StackPage
import org.gtk.gtk.StackPage.Companion.wrap
import org.gtk.gtk.asWidgetOrNull
import org.gtk.gtk.selection.SelectionModel
import org.gtk.gtk.selection.SelectionModel.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 16 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Stack.html">GtkStack</a>
 */
class Stack(
	val stackPointer: CPointer<GtkStack>,
) : Widget(stackPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_STACK))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Stack.new.html">
	 *     gtk_stack_new</a>
	 */
	constructor() : this(gtk_stack_new()!!.reinterpret())

	fun addChild(child: Widget): StackPage =
		gtk_stack_add_child(stackPointer, child.widgetPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.add_named.html">
	 *     gtk_stack_add_named</a>
	 */
	fun addNamed(name: String, child: Widget): StackPage =
		gtk_stack_add_named(stackPointer, child.widgetPointer, name)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.add_titled.html">
	 *     gtk_stack_add_titled</a>
	 */
	fun addTitled(name: String, title: String, child: Widget): StackPage =
		gtk_stack_add_titled(stackPointer, child.widgetPointer, name, title)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_child_by_name.html">
	 *     gtk_stack_get_child_by_name</a>
	 */
	fun getChildByName(name: String): Widget? =
		gtk_stack_get_child_by_name(stackPointer, name).asWidgetOrNull()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_hhomogeneous.html">
	 *     gtk_stack_get_hhomogeneous</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_hhomogeneous.html">
	 *     gtk_stack_set_hhomogeneous</a>
	 */
	var isHorizontallyHomogeneous: Boolean
		get() = gtk_stack_get_hhomogeneous(stackPointer).bool
		set(value) = gtk_stack_set_hhomogeneous(stackPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_interpolate_size.html">
	 *     gtk_stack_get_interpolate_size</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_interpolate_size.html">
	 *     gtk_stack_set_interpolate_size</a>
	 */
	var interpolateSize: Boolean
		get() = gtk_stack_get_interpolate_size(stackPointer).bool
		set(value) = gtk_stack_set_interpolate_size(stackPointer, value.gtk)

	fun getPage(child: Widget): StackPage =
		gtk_stack_get_page(stackPointer, child.widgetPointer)!!.wrap()

	val pages: SelectionModel
		get() = gtk_stack_get_pages(stackPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_transition_duration.html">
	 *     gtk_stack_get_transition_duration</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_transition_duration.html">
	 *     gtk_stack_set_transition_duration</a>
	 */
	var transitionDuration: UInt
		get() = gtk_stack_get_transition_duration(stackPointer)
		set(value) = gtk_stack_set_transition_duration(stackPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_transition_running.html">
	 *     gtk_stack_get_transition_running</a>
	 */
	val isTransitionRunning: Boolean
		get() = gtk_stack_get_transition_running(stackPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_transition_type.html">
	 *     gtk_stack_get_transition_type</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_transition_type.html">
	 *     gtk_stack_set_transition_type</a>
	 */
	var transitionType: TransitionType
		get() = TransitionType.valueOf(
			gtk_stack_get_transition_type(
				stackPointer
			)
		)!!
		set(value) = gtk_stack_set_transition_type(stackPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_vhomogeneous.html">
	 *     gtk_stack_get_vhomogeneous</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_vhomogeneous.html">
	 *     gtk_stack_set_vhomogeneous</a>
	 */
	var isVerticallyHomogeneous: Boolean
		get() = gtk_stack_get_vhomogeneous(stackPointer).bool
		set(value) = gtk_stack_set_vhomogeneous(stackPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_visible_child.html">
	 *     gtk_stack_get_visible_child</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_visible_child.html">
	 *     gtk_stack_set_visible_child</a>
	 */
	var visibleChild: Widget?
		get() = gtk_stack_get_visible_child(stackPointer).asWidgetOrNull()
		set(value) = gtk_stack_set_visible_child(
			stackPointer,
			value?.widgetPointer
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.get_visible_child_name.html">
	 *     gtk_stack_get_visible_child_name</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_visible_child_name.html">
	 *     gtk_stack_set_visible_child_name</a>
	 */
	var visibleChildName: String?
		get() = gtk_stack_get_visible_child_name(stackPointer)?.toKString()
		set(value) = gtk_stack_set_visible_child_name(stackPointer, value)

	fun remove(child: Widget) {
		gtk_stack_remove(stackPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Stack.set_visible_child_full.html">
	 *     gtk_stack_set_visible_child_full</a>
	 */
	fun setVisibleChildFull(name: String, transitionType: TransitionType) {
		gtk_stack_set_visible_child_full(stackPointer, name, transitionType.gtk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.StackTransitionType.html">
	 *     GtkStackTransitionType</a>
	 */
	enum class TransitionType(
		val gtk: GtkStackTransitionType,
	) {
		/** No transition */
		NONE(GTK_STACK_TRANSITION_TYPE_NONE),

		/** A cross-fade */
		CROSSFADE(GTK_STACK_TRANSITION_TYPE_CROSSFADE),

		/** Slide from left to right */
		SLIDE_RIGHT(GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT),

		/** Slide from right to left */
		SLIDE_LEFT(GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT),

		/** Slide from bottom up */
		SLIDE_UP(GTK_STACK_TRANSITION_TYPE_SLIDE_UP),

		/** Slide from top down */
		SLIDE_DOWN(GTK_STACK_TRANSITION_TYPE_SLIDE_DOWN),

		/** Slide from left or right according to the children order. */
		SLIDE_LEFT_RIGHT(GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT),

		/** Slide from top down or bottom up according to the order. */
		LEFT_RIGHT(GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT),

		/** Slide from top down or bottom up according to the order */
		SLIDE_UP_DOWN(GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN),

		/**
		 * Cover the old page by sliding up.
		 * @since 3.12
		 */
		OVER_UP(GTK_STACK_TRANSITION_TYPE_OVER_UP),

		/**
		 * Cover the old page by sliding down.
		 * @since 3.12
		 */
		OVER_DOWN(GTK_STACK_TRANSITION_TYPE_OVER_DOWN),

		/**
		 * Cover the old page by sliding to the left.
		 * @since 3.12
		 */
		OVER_LEFT(GTK_STACK_TRANSITION_TYPE_OVER_LEFT),

		/**
		 * Cover the old page by sliding to the right.
		 * @since 3.12
		 */
		OVER_RIGHT(GTK_STACK_TRANSITION_TYPE_OVER_RIGHT),

		/**
		 * Uncover the new page by sliding up.
		 * @since 3.12
		 */
		UNDER_UP(GTK_STACK_TRANSITION_TYPE_UNDER_UP),

		/**
		 * Uncover the new page by sliding down.
		 * @since 3.12
		 */
		UNDER_DOWN(GTK_STACK_TRANSITION_TYPE_UNDER_DOWN),

		/**
		 * Uncover the new page by sliding to the left.
		 * @since 3.12
		 */
		UNDER_LEFT(GTK_STACK_TRANSITION_TYPE_UNDER_LEFT),

		/**
		 * Uncover the new page by sliding to the right.
		 * @since 3.12
		 */
		UNDER_RIGHT(GTK_STACK_TRANSITION_TYPE_UNDER_RIGHT),

		/**
		 * Cover the old page sliding up or uncover the new page sliding down,
		 * according to order.
		 * @since 3.12
		 */
		OVER_UP_DOWN(GTK_STACK_TRANSITION_TYPE_OVER_UP_DOWN),

		/**
		 * Cover the old page sliding down or uncover the new page sliding up,
		 * according to order.
		 * @since 3.14
		 */
		OVER_DOWN_UP(GTK_STACK_TRANSITION_TYPE_OVER_DOWN_UP),

		/**
		 * Cover the old page sliding left or uncover the new page sliding right,
		 * according to order.
		 * @since 3.14
		 */
		OVER_LEFT_RIGHT(GTK_STACK_TRANSITION_TYPE_OVER_LEFT_RIGHT),

		/**
		 * Cover the old page sliding right or uncover the new page sliding left,
		 * according to order.
		 * @since 3.14
		 */
		OVER_RIGHT_LEFT(GTK_STACK_TRANSITION_TYPE_OVER_RIGHT_LEFT),

		/**
		 * Pretend the pages are sides of a cube and
		 * rotate that cube to the left.
		 */
		ROTATE_LEFT(GTK_STACK_TRANSITION_TYPE_ROTATE_LEFT),

		/**
		 * Pretend the pages are sides of a cube and
		 * rotate that cube to the right.
		 */
		ROTATE_RIGHT(GTK_STACK_TRANSITION_TYPE_ROTATE_RIGHT),

		/**
		 * Pretend the pages are sides of a cube and
		 * rotate that cube to the left or right according to the children order.
		 */
		ROTATE_LEFT_RIGHT(GTK_STACK_TRANSITION_TYPE_ROTATE_LEFT_RIGHT);

		companion object {
			fun valueOf(gtk: GtkStackTransitionType) =
				when (gtk) {
					GTK_STACK_TRANSITION_TYPE_NONE -> NONE
					GTK_STACK_TRANSITION_TYPE_CROSSFADE -> CROSSFADE
					GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT -> SLIDE_RIGHT
					GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT -> SLIDE_LEFT
					GTK_STACK_TRANSITION_TYPE_SLIDE_UP -> SLIDE_UP
					GTK_STACK_TRANSITION_TYPE_SLIDE_DOWN -> SLIDE_DOWN
					GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT -> SLIDE_LEFT_RIGHT
					GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN -> SLIDE_UP_DOWN
					GTK_STACK_TRANSITION_TYPE_OVER_UP -> OVER_UP
					GTK_STACK_TRANSITION_TYPE_OVER_DOWN -> OVER_DOWN
					GTK_STACK_TRANSITION_TYPE_OVER_LEFT -> OVER_LEFT
					GTK_STACK_TRANSITION_TYPE_OVER_RIGHT -> OVER_RIGHT
					GTK_STACK_TRANSITION_TYPE_UNDER_UP -> UNDER_UP
					GTK_STACK_TRANSITION_TYPE_UNDER_DOWN -> UNDER_DOWN
					GTK_STACK_TRANSITION_TYPE_UNDER_LEFT -> UNDER_LEFT
					GTK_STACK_TRANSITION_TYPE_UNDER_RIGHT -> UNDER_RIGHT
					GTK_STACK_TRANSITION_TYPE_OVER_UP_DOWN -> OVER_UP_DOWN
					GTK_STACK_TRANSITION_TYPE_OVER_DOWN_UP -> OVER_DOWN_UP
					GTK_STACK_TRANSITION_TYPE_OVER_LEFT_RIGHT -> OVER_LEFT_RIGHT
					GTK_STACK_TRANSITION_TYPE_OVER_RIGHT_LEFT -> OVER_RIGHT_LEFT
					GTK_STACK_TRANSITION_TYPE_ROTATE_LEFT -> ROTATE_LEFT
					GTK_STACK_TRANSITION_TYPE_ROTATE_RIGHT -> ROTATE_RIGHT
					GTK_STACK_TRANSITION_TYPE_ROTATE_LEFT_RIGHT -> ROTATE_LEFT_RIGHT
				}
		}
	}

	companion object {
		inline fun CPointer<GtkStack>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkStack>.wrap() =
			Stack(this)
	}
}