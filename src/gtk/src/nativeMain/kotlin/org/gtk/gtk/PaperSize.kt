package org.gtk.gtk

import gtk.GtkPaperSize_autoptr
import gtk.gtk_paper_size_new

/**
 * 06 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/struct.PaperSize.html"></a>
 */
class PaperSize(
	val sizePointer: GtkPaperSize_autoptr,
) {
	constructor(name: String) :
			this(gtk_paper_size_new(name)!!)

	companion object{
		inline fun GtkPaperSize_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkPaperSize_autoptr.wrap() =
			PaperSize(this)
	}
}