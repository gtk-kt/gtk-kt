package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkTreeViewColumnSizing.*
import kotlinx.cinterop.*
import org.gtk.glib.asStablePointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.*
import org.gtk.gtk.*
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gtk.common.enums.SortType
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * 31 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.TreeViewColumn.html">
 *     GtkTreeViewColumn</a>
 */
class TreeViewColumn(
	val tree_column: GtkTreeViewColumn_autoptr,
) : KGObject(tree_column.reinterpret()), Buildable, CellLayout {

	override val buildablePointer: GtkBuildable_autoptr =
		tree_column.reinterpret()

	override val cellLayoutHolder: GtkCellLayout_autoptr =
		tree_column.reinterpret()

	constructor() : this(gtk_tree_view_column_new()!!)

	constructor(area: CellArea) : this(gtk_tree_view_column_new_with_area(
		area.cellAreaPointer
	)!!)

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_TREE_VIEW_COLUMN
	))

	// ignore gtk_tree_view_column_new_with_attributes vararg

	override fun addAttribute(
		@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
		cellRenderer: CellRenderer,
		attribute: String,
		column: Int,
	) {
		gtk_tree_view_column_add_attribute(
			tree_column,
			cellRenderer.cellRendererPointer,
			attribute,
			column
		)
	}

	data class CellPosition(
		val xOffset: Int,
		val width: Int,
	)

	fun cellGetPosition(
		cellRenderer: CellRenderer,
	): CellPosition = memScoped {
		val xOffset = alloc<IntVar>()
		val width = alloc<IntVar>()

		gtk_tree_view_column_cell_get_position(
			tree_column,
			cellRenderer.cellRendererPointer,
			xOffset.ptr,
			width.ptr
		)

		CellPosition(
			xOffset.value,
			width.value
		)
	}

	data class CellSize(
		val xOffset: Int,
		val yOffset: Int,
		val width: Int,
		val height: Int,
	)

	fun cellGetSize(): CellSize = memScoped {
		val xOffset = alloc<IntVar>()
		val yOffset = alloc<IntVar>()
		val width = alloc<IntVar>()
		val height = alloc<IntVar>()

		gtk_tree_view_column_cell_get_size(
			tree_column,
			xOffset.ptr,
			yOffset.ptr,
			width.ptr,
			height.ptr
		)

		CellSize(
			xOffset.value,
			yOffset.value,
			width.value,
			height.value
		)
	}

	val cellIsVisible: Boolean
		get() = gtk_tree_view_column_cell_is_visible(tree_column).bool

	fun cellSetCellData(
		treeModel: TreeModel,
		iter: TreeIter,
		isExpander: Boolean,
		isExpanded: Boolean,
	) {
		gtk_tree_view_column_cell_set_cell_data(
			tree_column,
			treeModel.treeModelPointer,
			iter.treeIterPointer,
			isExpander.gtk,
			isExpanded.gtk
		)
	}

	override fun clear() {
		gtk_tree_view_column_clear(tree_column)
	}

	override fun clearAttributes(
		cell: CellRenderer,
	) {
		gtk_tree_view_column_clear_attributes(
			tree_column,
			cell.cellRendererPointer
		)
	}

	fun clicked() {
		gtk_tree_view_column_clicked(tree_column)
	}

	fun focusCell(cell: CellRenderer) {
		gtk_tree_view_column_focus_cell(
			tree_column,
			cell.cellRendererPointer
		)
	}

	var alignment: Float
		get() = gtk_tree_view_column_get_alignment(tree_column)
		set(value) = gtk_tree_view_column_set_alignment(tree_column, value)

	val button: Widget
		get() = gtk_tree_view_column_get_button(tree_column)!!.wrap()

	var clickable: Boolean
		get() = gtk_tree_view_column_get_clickable(tree_column).bool
		set(value) = gtk_tree_view_column_set_clickable(tree_column, value.gtk)

	var expand: Boolean
		get() = gtk_tree_view_column_get_expand(tree_column).bool
		set(value) = gtk_tree_view_column_set_expand(tree_column, value.gtk)

	var fixedWidth: Int
		get() = gtk_tree_view_column_get_fixed_width(tree_column)
		set(value) = gtk_tree_view_column_set_fixed_width(tree_column, value)

	var maxWidth: Int
		get() = gtk_tree_view_column_get_max_width(tree_column)
		set(value) = gtk_tree_view_column_set_max_width(tree_column, value)

	var minWidth: Int
		get() = gtk_tree_view_column_get_min_width(tree_column)
		set(value) = gtk_tree_view_column_set_min_width(tree_column, value)

	var reorderable: Boolean
		get() = gtk_tree_view_column_get_reorderable(tree_column).bool
		set(value) = gtk_tree_view_column_set_reorderable(tree_column, value.gtk)

	var resizable: Boolean
		get() = gtk_tree_view_column_get_resizable(tree_column).bool
		set(value) = gtk_tree_view_column_set_resizable(tree_column, value.gtk)

	var sortColumnId: Int
		get() = gtk_tree_view_column_get_sort_column_id(tree_column)
		set(value) = gtk_tree_view_column_set_sort_column_id(tree_column, value)

	var sortIndicator: Boolean
		get() = gtk_tree_view_column_get_sort_indicator(tree_column).bool
		set(value) = gtk_tree_view_column_set_sort_indicator(
			tree_column,
			value.gtk
		)

	var sortOrder: SortType
		get() = SortType.valueOf(
			gtk_tree_view_column_get_sort_order(tree_column)
		)
		set(value) = gtk_tree_view_column_set_sort_order(tree_column, value.gtk)

	var spacing: Int
		get() = gtk_tree_view_column_get_spacing(tree_column)
		set(value) = gtk_tree_view_column_set_spacing(tree_column, value)

	var title: String
		get() = gtk_tree_view_column_get_title(tree_column)!!.toKString()
		set(value) = gtk_tree_view_column_set_title(tree_column, value)

	val treeView: Widget
		get() = gtk_tree_view_column_get_tree_view(tree_column)!!.wrap()

	var visible: Boolean
		get() = gtk_tree_view_column_get_visible(tree_column).bool
		set(value) = gtk_tree_view_column_set_visible(
			tree_column,
			value.gtk
		)

	var widget: Widget
		get() = gtk_tree_view_column_get_widget(tree_column)!!.wrap()
		set(value) = gtk_tree_view_column_set_widget(
			tree_column,
			value.widgetPointer
		)

	val width: Int
		get() = gtk_tree_view_column_get_width(tree_column)

	val xOffset: Int
		get() = gtk_tree_view_column_get_x_offset(tree_column)

	override fun packEnd(cell: CellRenderer, expand: Boolean) {
		gtk_tree_view_column_pack_end(
			tree_column,
			cell.cellRendererPointer,
			expand.gtk
		)
	}

	override fun packStart(cell: CellRenderer, expand: Boolean) {
		gtk_tree_view_column_pack_start(
			tree_column,
			cell.cellRendererPointer,
			expand.gtk
		)
	}

	fun resize() {
		gtk_tree_view_column_queue_resize(tree_column)
	}

	// ignore gtk_tree_view_column_set_attributes vararg

	fun setCellDataFunc(
		cellRenderer: CellRenderer,
		func: TreeCellDataFunc?,
	) {
		if (func == null) {
			gtk_tree_view_column_set_cell_data_func(
				tree_column,
				cellRenderer.cellRendererPointer,
				null,
				null,
				null
			)
			return
		}
		gtk_tree_view_column_set_cell_data_func(
			tree_column,
			cellRenderer.cellRendererPointer,
			staticTreeCellDataFunc,
			func.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun setSizing(type: Sizing) {
		gtk_tree_view_column_set_sizing(tree_column, type.gtk)
	}

	fun addOnClickedCallback(action: TypedNoArgFunc<TreeViewColumn>) =
		addSignalCallback(Signals.CLICKED, action, staticNoArgGCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.TreeViewColumnSizing.html">
	 *     Sizing</a>
	 */
	enum class Sizing(
		val gtk: GtkTreeViewColumnSizing,
	) {
		GROW_ONLY(GTK_TREE_VIEW_COLUMN_GROW_ONLY),
		AUTOSIZE(GTK_TREE_VIEW_COLUMN_AUTOSIZE),
		FIXED(GTK_TREE_VIEW_COLUMN_FIXED);

		companion object {
			fun valueOf(gtk: GtkTreeViewColumnSizing) =
				when (gtk) {
					GTK_TREE_VIEW_COLUMN_GROW_ONLY -> GROW_ONLY
					GTK_TREE_VIEW_COLUMN_AUTOSIZE -> AUTOSIZE
					GTK_TREE_VIEW_COLUMN_FIXED -> FIXED
				}
		}
	}

	companion object {
		inline fun GtkTreeViewColumn_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkTreeViewColumn_autoptr.wrap() =
			TreeViewColumn(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: GtkTreeViewColumn_autoptr,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<TreeViewColumn>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()
	}
}