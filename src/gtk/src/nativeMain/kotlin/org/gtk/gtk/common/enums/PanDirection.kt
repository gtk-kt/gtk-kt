package org.gtk.gtk.common.enums

import gtk.GtkPanDirection
import gtk.GtkPanDirection.*

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.PanDirection.html">
 *     GtkPanDirection</a>
 */
enum class PanDirection(
	val gtk: GtkPanDirection,
) {
	LEFT(GTK_PAN_DIRECTION_LEFT),
	RIGHT(GTK_PAN_DIRECTION_RIGHT),
	UP(GTK_PAN_DIRECTION_UP),
	DOWN(GTK_PAN_DIRECTION_DOWN);

	companion object {
		fun valueOf(gtk: GtkPanDirection) =
			when (gtk) {
				GTK_PAN_DIRECTION_LEFT -> LEFT
				GTK_PAN_DIRECTION_RIGHT -> RIGHT
				GTK_PAN_DIRECTION_UP -> UP
				GTK_PAN_DIRECTION_DOWN -> DOWN
			}
	}
}