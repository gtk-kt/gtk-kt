package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk

/**
 * 21 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EditableLabel.html">
 *     GtkEditableLabel</a>
 */
class EditableLabel(
	val editableLabelPointer: GtkEditableLabel_autoptr,
) : Widget(editableLabelPointer.reinterpret()) {
	constructor(label: String) :
			this(gtk_editable_label_new(label)!!.reinterpret())

	val editing: Boolean
		get() = gtk_editable_label_get_editing(editableLabelPointer).bool


	fun startEditing() {
		gtk_editable_label_start_editing(editableLabelPointer)
	}

	fun stopEditing(commit: Boolean = true) {
		gtk_editable_label_stop_editing(editableLabelPointer, commit.gtk)
	}
}