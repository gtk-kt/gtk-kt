package org.gtk.gtk.cellrenderer

import gtk.GTK_TYPE_CELL_RENDERER_PIXBUF
import gtk.GtkCellRendererPixbuf_autoptr
import gtk.gtk_cell_renderer_pixbuf_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererPixbuf.html">
 *     GtkCellRendererPixbuf</a>
 */
class CellRendererPixbuf(
	val pixBufPointer: GtkCellRendererPixbuf_autoptr,
) : CellRenderer(pixBufPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_pixbuf_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_PIXBUF
	))

}