package org.gtk.gtk

import gtk.*
import gtk.GtkSizeGroupMode.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.WrappedSList
import org.gtk.glib.WrappedSList.Companion.asWrappedSList
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 20 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SizeGroup.html">GtkSizeGroup</a>
 */
class SizeGroup(
	val sizeGroupPointer: CPointer<GtkSizeGroup>
) : KGObject(sizeGroupPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.SizeGroup.new.html">gtk_size_group_new</a>
	 */
	constructor(mode: Mode) : this(gtk_size_group_new(mode.gtk)!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SizeGroup.get_mode.html">gtk_size_group_get_mode</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SizeGroup.set_mode.html">gtk_size_group_set_mode</a>
	 */
	var mode: Mode
		get() = Mode.valueOf(
			gtk_size_group_get_mode(sizeGroupPointer)
		)!!
		set(value) =
			gtk_size_group_set_mode(sizeGroupPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SizeGroup.add_widget.html">gtk_size_group_add_widget</a>
	 */
	fun addWidget(widget: Widget) {
		gtk_size_group_add_widget(sizeGroupPointer, widget.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SizeGroup.remove_widget.html">gtk_size_group_remove_widget</a>
	 */
	fun removeWidget(widget: Widget) {
		gtk_size_group_remove_widget(sizeGroupPointer, widget.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SizeGroup.get_widgets.html">gtk_size_group_get_widgets</a>
	 */
	val widgets: WrappedSList<Widget>
		get() = gtk_size_group_get_widgets(sizeGroupPointer)!!.asWrappedSList(
			{ reinterpret<GtkWidget>().wrap() },
			{ widgetPointer }
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.SizeGroupMode.html">GtkSizeGroupMode</a>
	 */
	enum class Mode(val gtk: GtkSizeGroupMode) {
		NONE(GTK_SIZE_GROUP_NONE),
		HORIZONTAL(GTK_SIZE_GROUP_HORIZONTAL),
		VERTICAL(GTK_SIZE_GROUP_VERTICAL),
		BOTH(GTK_SIZE_GROUP_BOTH);

		companion object {

			fun valueOf(gtk: GtkSizeGroupMode) =
				values().find { it.gtk == gtk }
		}
	}
}