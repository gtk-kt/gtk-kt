package org.gtk.gtk.widgets.scrolledwindow

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkCornerType.*
import gtk.GtkPolicyType.*
import gtk.GtkPolicyType.Var
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.*
import org.gtk.gtk.Adjustment
import org.gtk.gtk.common.enums.DirectionType
import org.gtk.gtk.common.enums.PositionType
import org.gtk.gtk.common.enums.ScrollType
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ScrolledWindow.html">
 *     GtkScrolledWindow</a>
 */
class ScrolledWindow(
	val scrolledWindowPointer: GtkScrolledWindow_autoptr,
) : Widget(scrolledWindowPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SCROLLED_WINDOW))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ScrolledWindow.new.html">
	 *     gtk_scrolled_window_new</a>
	 */
	constructor() : this(gtk_scrolled_window_new()!!.reinterpret())

	var child: Widget?
		get() = gtk_scrolled_window_get_child(scrolledWindowPointer).wrap()
		set(value) = gtk_scrolled_window_set_child(
			scrolledWindowPointer,
			value?.widgetPointer
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_hadjustment.html">
	 *     gtk_scrolled_window_get_hadjustment</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_hadjustment.html">
	 *     gtk_scrolled_window_set_hadjustment</a>
	 */
	var horizontalAdjustment: Adjustment?
		get() = gtk_scrolled_window_get_hadjustment(scrolledWindowPointer)?.let {
			Adjustment(
				it
			)
		}
		set(value) = gtk_scrolled_window_set_hadjustment(
			scrolledWindowPointer,
			value?.adjustmentPointer
		)

	var hasFrame: Boolean
		get() = gtk_scrolled_window_get_has_frame(scrolledWindowPointer).bool
		set(value) = gtk_scrolled_window_set_has_frame(scrolledWindowPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_hscrollbar.html">
	 *     gtk_scrolled_window_get_hscrollbar</a>
	 */
	val horizontalScrollBar: Widget
		get() = Widget(gtk_scrolled_window_get_hscrollbar(scrolledWindowPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_kinetic_scrolling.html">
	 *     gtk_scrolled_window_get_kinetic_scrolling</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_kinetic_scrolling.html">
	 *     gtk_scrolled_window_set_kinetic_scrolling</a>
	 */
	var kineticScrolling: Boolean
		get() = gtk_scrolled_window_get_kinetic_scrolling(
			scrolledWindowPointer
		)
			.bool
		set(value) = gtk_scrolled_window_set_kinetic_scrolling(
			scrolledWindowPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_max_content_height.html">
	 *     gtk_scrolled_window_get_max_content_height</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_max_content_height.html">
	 *     gtk_scrolled_window_set_max_content_height</a>
	 */
	var maxContentHeight: Int
		get() = gtk_scrolled_window_get_max_content_height(scrolledWindowPointer)
		set(value) = gtk_scrolled_window_set_max_content_height(
			scrolledWindowPointer,
			value
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_max_content_height.html">
	 *     gtk_scrolled_window_get_max_content_width</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_max_content_height.html">
	 *     gtk_scrolled_window_set_max_content_width</a>
	 */
	var maxContentWidth: Int
		get() = gtk_scrolled_window_get_max_content_width(scrolledWindowPointer)
		set(value) = gtk_scrolled_window_set_max_content_width(
			scrolledWindowPointer,
			value
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_min_content_height.html">
	 *     gtk_scrolled_window_get_min_content_height</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_min_content_height.html">
	 *     gtk_scrolled_window_set_min_content_height</a>
	 */
	var minContentHeight: Int
		get() = gtk_scrolled_window_get_min_content_height(scrolledWindowPointer)
		set(value) = gtk_scrolled_window_set_min_content_height(
			scrolledWindowPointer,
			value
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_min_content_width.html">
	 *     gtk_scrolled_window_get_min_content_width</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_min_content_width.html">
	 *     gtk_scrolled_window_set_min_content_width</a>
	 */
	var minContentWidth: Int
		get() = gtk_scrolled_window_get_min_content_width(scrolledWindowPointer)
		set(value) = gtk_scrolled_window_set_min_content_width(
			scrolledWindowPointer,
			value
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_overlay_scrolling.html">
	 *     gtk_scrolled_window_get_overlay_scrolling</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_overlay_scrolling.html">
	 *     gtk_scrolled_window_set_overlay_scrolling</a>
	 */
	var overlayScrolling: Boolean
		get() = gtk_scrolled_window_get_overlay_scrolling(
			scrolledWindowPointer
		).bool
		set(value) = gtk_scrolled_window_set_overlay_scrolling(
			scrolledWindowPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_placement.html">
	 *     gtk_scrolled_window_get_placement</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_placement.html">
	 *     gtk_scrolled_window_set_placement</a>
	 */
	var placement: CornerType
		get() = CornerType.valueOf(
			gtk_scrolled_window_get_placement(
				scrolledWindowPointer
			)
		)
		set(value) = gtk_scrolled_window_set_placement(
			scrolledWindowPointer,
			value.gtk
		)

	data class Policies(
		val horizontal: PolicyType,
		val vertical: PolicyType,
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_policy.html">
	 *     gtk_scrolled_window_get_policy</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_policy.html">
	 *     gtk_scrolled_window_set_policy</a>
	 */
	var policy: Policies
		get() {
			val hP = cValue<Var>()
			val vP = cValue<Var>()

			gtk_scrolled_window_get_policy(scrolledWindowPointer, hP, vP)

			return memScoped {
				Policies(
					PolicyType.valueOf(hP.ptr.pointed.value),
					PolicyType.valueOf(vP.ptr.pointed.value)
				)
			}
		}
		set(value) {
			gtk_scrolled_window_set_policy(
				scrolledWindowPointer,
				value.horizontal.gtk,
				value.vertical.gtk
			)
		}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_propagate_natural_height.html">
	 *     gtk_scrolled_window_get_propagate_natural_height</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_propagate_natural_height.html">
	 *     gtk_scrolled_window_set_propagate_natural_height</a>
	 */
	var propagateNaturalHeight: Boolean
		get() = gtk_scrolled_window_get_propagate_natural_height(
			scrolledWindowPointer
		)
			.bool
		set(value) = gtk_scrolled_window_set_propagate_natural_height(
			scrolledWindowPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_propagate_natural_width.html">
	 *     gtk_scrolled_window_get_propagate_natural_width</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_propagate_natural_width.html">
	 *     gtk_scrolled_window_set_propagate_natural_width</a>
	 */
	var propagateNaturalWidth: Boolean
		get() = gtk_scrolled_window_get_propagate_natural_width(
			scrolledWindowPointer
		)
			.bool
		set(value) = gtk_scrolled_window_set_propagate_natural_width(
			scrolledWindowPointer,
			value.gtk
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_vadjustment.html">
	 *     gtk_scrolled_window_get_vadjustment</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.set_vadjustment.html">
	 *     gtk_scrolled_window_set_vadjustment</a>
	 */
	var verticalAdjustment: Adjustment?
		get() = gtk_scrolled_window_get_vadjustment(scrolledWindowPointer)?.let {
			Adjustment(
				it
			)
		}
		set(value) = gtk_scrolled_window_set_vadjustment(
			scrolledWindowPointer,
			value?.adjustmentPointer
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.get_vscrollbar.html">
	 *     gtk_scrolled_window_get_vscrollbar</a>
	 */
	val verticalScrollBar: Widget
		get() = Widget(gtk_scrolled_window_get_vscrollbar(scrolledWindowPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ScrolledWindow.unset_placement.html">
	 *     gtk_scrolled_window_unset_placement</a>
	 */
	fun unsetPlacement() {
		gtk_scrolled_window_unset_placement(scrolledWindowPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.PolicyType.html">
	 *     GtkPolicyType</a>
	 */
	enum class PolicyType(val gtk: GtkPolicyType) {

		/**
		 * The scrollbar is always visible.
		 *
		 * The view size is independent of the content.
		 */
		ALWAYS(GTK_POLICY_ALWAYS),

		/**
		 * The scrollbar will appear and disappear as necessary.
		 *
		 * For example, when all of a [org.gtk.gtk.widgets.container.TreeView] can not be seen.
		 */
		AUTOMATIC(GTK_POLICY_AUTOMATIC),

		/**
		 * The scrollbar should never appear.
		 *
		 * In this mode the content determines the size.
		 */
		NEVER(GTK_POLICY_NEVER),

		/**
		 * Don't show a scrollbar, but don't force the size to follow the content.
		 *
		 * This can be used e.g. to make multiple scrolled windows share a scrollbar.
		 *
		 * @since 3.16
		 */
		EXTERNAL(GTK_POLICY_EXTERNAL);

		companion object {

			fun valueOf(gtk: GtkPolicyType) =
				when (gtk) {
					GTK_POLICY_ALWAYS -> ALWAYS
					GTK_POLICY_AUTOMATIC -> AUTOMATIC
					GTK_POLICY_NEVER -> NEVER
					GTK_POLICY_EXTERNAL -> EXTERNAL
				}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.CornerType.html">
	 *     GtkCornerType</a>
	 */
	enum class CornerType(val gtk: GtkCornerType) {

		/** Place the scrollbars on the right and bottom of the widget (default behaviour). */
		TOP_LEFT(GTK_CORNER_TOP_LEFT),

		/** Place the scrollbars on the top and right of the widget. */
		BOTTOM_LEFT(GTK_CORNER_BOTTOM_LEFT),

		/** Place the scrollbars on the left and bottom of the widget. */
		TOP_RIGHT(GTK_CORNER_TOP_RIGHT),

		/** Place the scrollbars on the top and left of the widget. */
		BOTTOM_RIGHT(GTK_CORNER_BOTTOM_RIGHT);

		companion object {

			fun valueOf(gtk: GtkCornerType) =
				when (gtk) {
					GTK_CORNER_TOP_LEFT -> TOP_LEFT
					GTK_CORNER_BOTTOM_LEFT -> BOTTOM_LEFT
					GTK_CORNER_TOP_RIGHT -> TOP_RIGHT
					GTK_CORNER_BOTTOM_RIGHT -> BOTTOM_RIGHT
				}
		}
	}


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ScrolledWindow.edge-overshot.html">
	 *     edge-overshot</a>
	 */
	fun addOnEdgeOvershotCallback(action: ScrolledWindow.(PositionType) -> Unit) =
		addSignalCallback(Signals.EDGE_OVERSHOT, action, staticPositionTypeCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ScrolledWindow.edge-reached.html">
	 *     edge-reached</a>
	 */
	fun addOnEdgeReachedCallback(action: ScrolledWindow. (PositionType) -> Unit) =
		addSignalCallback(Signals.EDGE_REACHED, action, staticPositionTypeCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ScrolledWindow.move-focus-out.html">
	 *     move-focus-out</a>
	 */
	fun addOnMoveFocusOutCallback(action: ScrolledWindow.(DirectionType) -> Unit) =
		addSignalCallback(Signals.MOVE_FOCUS_OUT, action, staticDirectionTypeCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.ScrolledWindow.scroll-child.html">
	 *     scroll-child</a>
	 */
	fun setScrollChildCallback(action: ScrollChildFunction) =
		addSignalCallback(Signals.SCROLL_CHILD, action, staticScrollChildFunction)

	companion object {
		inline fun GtkScrolledWindow_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkScrolledWindow_autoptr.wrap() =
			ScrolledWindow(this)

		private val staticPositionTypeCallback: GCallback =
			staticCFunction {
					self: GtkScrolledWindow_autoptr,
					pos: GtkPositionType,
					data: gpointer,
				->
				data.asStableRef<ScrolledWindow.(PositionType) -> Unit>()
					.get()
					.invoke(self.wrap(), PositionType.valueOf(pos))
			}.reinterpret()

		private val staticDirectionTypeCallback: GCallback =
			staticCFunction {
					self: GtkScrolledWindow_autoptr,
					direction: GtkDirectionType,
					data: gpointer,
				->
				data.asStableRef<ScrolledWindow.(DirectionType) -> Unit>()
					.get()
					.invoke(self.wrap(), DirectionType.valueOf(direction))
				Unit
			}.reinterpret()

		private val staticScrollChildFunction: GCallback =
			staticCFunction {
					self: GtkScrolledWindow_autoptr,
					scroll: GtkScrollType,
					horizontal: gboolean,
					data: gpointer,
				->
				data.asStableRef<ScrollChildFunction>()
					.get()
					.invoke(
						self.wrap(),
						ScrollType.valueOf(scroll),
						horizontal.bool
					).gtk
			}.reinterpret()
	}
}

typealias ScrollChildFunction =
		ScrolledWindow.(
			ScrollType,
			isHorizontal: Boolean,
		) -> Boolean