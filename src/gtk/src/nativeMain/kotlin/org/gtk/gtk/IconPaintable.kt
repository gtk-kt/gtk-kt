package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gdk.Paintable
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 16 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IconPaintable.html">
 *     GtkIconPaintable</a>
 */
class IconPaintable(
	val iconPaintPointer: GtkIconPaintable_autoptr,
) : KGObject(iconPaintPointer.reinterpret()), Paintable {

	constructor(file: File, size: Int, scale: Int) :
			this(gtk_icon_paintable_new_for_file(
				file.filePointer,
				size,
				scale
			)!!)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_ICON_PAINTABLE
	))

	override val paintablePointer: CPointer<GdkPaintable> by lazy {
		iconPaintPointer.reinterpret()
	}

	val file: File?
		get() = gtk_icon_paintable_get_file(iconPaintPointer).wrap()

	val iconName: String?
		get() = gtk_icon_paintable_get_icon_name(iconPaintPointer)?.toKString()

	val isSymbolic: Boolean
		get() = gtk_icon_paintable_is_symbolic(iconPaintPointer).bool

	companion object{
		inline fun GtkIconPaintable_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkIconPaintable_autoptr.wrap() =
			IconPaintable(this)
	}
}