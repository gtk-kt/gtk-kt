package org.gtk.gtk.widgets

import gtk.GTK_TYPE_LIST_BASE
import gtk.GtkListBase
import gtk.GtkOrientable
import gtk.GtkScrollable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Orientable
import org.gtk.gtk.Scrollable

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ListBase.html">
 *     GtkListBase</a>
 */
open class ListBase(
	val listBasePointer: CPointer<GtkListBase>,
) : Widget(listBasePointer.reinterpret()), Scrollable, Orientable {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_LIST_BASE))

	override val orientablePointer: CPointer<GtkOrientable>
		get() = listBasePointer.reinterpret()

	override val scrollablePointer: CPointer<GtkScrollable>
		get() = listBasePointer.reinterpret()
}