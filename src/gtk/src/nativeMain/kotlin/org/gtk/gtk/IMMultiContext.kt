package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 16 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IMMulticontext.html">
 *     GtkIMMulticontext</a>
 */
class IMMultiContext(
	val simplePointer: GtkIMMulticontext_autoptr,
) : IMContext(simplePointer.reinterpret()) {

	constructor() : this(gtk_im_multicontext_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_IM_MULTICONTEXT
	))

	var contextId: String
		get() = gtk_im_multicontext_get_context_id(simplePointer)!!.toKString()
		set(value) = gtk_im_multicontext_set_context_id(simplePointer, value)
}