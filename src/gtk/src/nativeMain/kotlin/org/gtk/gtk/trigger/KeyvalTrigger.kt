package org.gtk.gtk.trigger

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 02 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.KeyvalTrigger.html">
 *     GtkKeyvalTrigger</a>
 */
class KeyvalTrigger(
	val keyvalTrigger: GtkKeyvalTrigger_autoptr,
) : ShortcutTrigger(keyvalTrigger.reinterpret()) {

	constructor(
		keyval: UInt,
		modifiers: GdkModifierType,
	) : this(gtk_keyval_trigger_new(keyval, modifiers)!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_KEYVAL_TRIGGER))

	val keyval: UInt
		get() = gtk_keyval_trigger_get_keyval(keyvalTrigger)

	val modifiers: GdkModifierType
		get() = gtk_keyval_trigger_get_modifiers(keyvalTrigger)
}