package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.pixbuf.Pixbuf
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Picture.html">
 *     GtkPicture</a>
 */
class Picture(
	val picturePointer: GtkPicture_autoptr,
) : Widget(picturePointer.reinterpret()) {

	constructor() : this(gtk_picture_new()!!.reinterpret())

	constructor(file: File?) :
			this(gtk_picture_new_for_file(file?.filePointer)!!.reinterpret())

	constructor(fileName: String?) :
			this(gtk_picture_new_for_filename(fileName)!!.reinterpret())

	constructor(paintable: Paintable?) :
			this(
				gtk_picture_new_for_paintable(
					paintable?.paintablePointer
				)!!.reinterpret()
			)

	constructor(pixbuf: Pixbuf?) :
			this(
				gtk_picture_new_for_pixbuf(
					pixbuf?.pixbufPointer
				)!!.reinterpret()
			)

	constructor(resourcePath: String?, isResource: Boolean) :
			this(gtk_picture_new_for_resource(resourcePath)!!.reinterpret())

	var alternativeText: String?
		get() = gtk_picture_get_alternative_text(picturePointer)?.toKString()
		set(value) = gtk_picture_set_alternative_text(picturePointer, value)

	var canShrink: Boolean
		get() = gtk_picture_get_can_shrink(picturePointer).bool
		set(value) = gtk_picture_set_can_shrink(picturePointer, value.gtk)

	var file: File?
		get() = gtk_picture_get_file(picturePointer).wrap()
		set(value) = gtk_picture_set_file(picturePointer, value?.filePointer)

	var keepAspectRatio: Boolean
		get() = gtk_picture_get_keep_aspect_ratio(picturePointer).bool
		set(value) = gtk_picture_set_keep_aspect_ratio(picturePointer, value.gtk)

	var paintable: Paintable?
		get() = gtk_picture_get_paintable(picturePointer).wrap()
		set(value) = gtk_picture_set_paintable(
			picturePointer,
			value?.paintablePointer
		)

	fun setFilename(fileName: String?) {
		gtk_picture_set_filename(picturePointer, fileName)
	}

	fun setPixbuf(pixbuf: Pixbuf?) {
		gtk_picture_set_pixbuf(picturePointer, pixbuf?.pixbufPointer)
	}

	fun setResource(resourcePath: String?) {
		gtk_picture_set_resource(picturePointer, resourcePath)
	}
}