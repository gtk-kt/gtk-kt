package org.gtk.gtk.cellrenderer

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_CELL_RENDERER_COMBO
import gtk.GtkCellRendererCombo_autoptr
import gtk.GtkTreeIter_autoptr
import gtk.gtk_cell_renderer_combo_new
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.TreeIter
import org.gtk.gtk.TreeIter.Companion.wrap
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererCombo.html">
 *     GtkCellRendererCombo</a>
 */
class CellRendererCombo(
	val comboPointer: GtkCellRendererCombo_autoptr,
) : CellRenderer(comboPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_combo_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_COMBO
	))


	fun addOnChangedCallback(action: CellRendererComboChangedFunc) =
		addSignalCallback(Signals.CHANGED,action, staticChangedCallback)


	companion object {
		inline fun GtkCellRendererCombo_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellRendererCombo_autoptr.wrap() =
			CellRendererCombo(this)

		private val staticChangedCallback: GCallback =
			staticCFunction {
					self: GtkCellRendererCombo_autoptr,
					path: CStringPointer,
					new_iter: GtkTreeIter_autoptr,
					data: gpointer,
				->
				data.asStableRef<CellRendererComboChangedFunc>()
					.get()
					.invoke(
						self.wrap(),
						path.toKString(),
						new_iter.wrap()
					)
			}.reinterpret()
	}
}

typealias CellRendererComboChangedFunc =
		CellRendererCombo.(
			path: String,
			newIter: TreeIter,
		) -> Unit