package org.gtk.gtk.widgets

import gtk.GtkMediaControls_autoptr
import gtk.gtk_media_controls_get_media_stream
import gtk.gtk_media_controls_new
import gtk.gtk_media_controls_set_media_stream
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.MediaStream
import org.gtk.gtk.MediaStream.Companion.wrap

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MediaControls.html">
 *     GtkMediaControls</a>
 */
class MediaControls(
	val mediaControlsPointer: GtkMediaControls_autoptr,
) : Widget(mediaControlsPointer.reinterpret()) {

	constructor(mediaStream: MediaStream?) :
			this(
				gtk_media_controls_new(
					mediaStream?.mediaStreamPointer
				)!!.reinterpret()
			)

	var mediaStream: MediaStream?
		get() = gtk_media_controls_get_media_stream(mediaControlsPointer).wrap()
		set(value) = gtk_media_controls_set_media_stream(
			mediaControlsPointer,
			value?.mediaStreamPointer
		)
}