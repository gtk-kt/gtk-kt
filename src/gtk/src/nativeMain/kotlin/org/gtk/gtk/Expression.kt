package org.gtk.gtk

import gtk.GtkExpression
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href=""></a>
 */
class Expression(val expressionPointer: CPointer<GtkExpression>) :
	KGObject(expressionPointer.reinterpret()) {


		companion object{
			inline fun CPointer<GtkExpression>?.wrap() =
				this?.wrap()

			inline fun CPointer<GtkExpression>.wrap() =
				Expression(this)
		}
}