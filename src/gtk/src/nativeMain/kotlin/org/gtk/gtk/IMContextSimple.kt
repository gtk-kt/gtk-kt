package org.gtk.gtk

import gtk.GTK_TYPE_IM_CONTEXT_SIMPLE
import gtk.GtkIMContextSimple_autoptr
import gtk.gtk_im_context_simple_add_compose_file
import gtk.gtk_im_context_simple_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 16 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IMContextSimple.html">
 *     GtkIMContextSimple</a>
 */
class IMContextSimple(
	val simplePointer: GtkIMContextSimple_autoptr,
) : IMContext(simplePointer.reinterpret()) {

	constructor() : this(gtk_im_context_simple_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_IM_CONTEXT_SIMPLE
	))

	fun addComposeFile(file: String) {
		gtk_im_context_simple_add_compose_file(
			simplePointer,
			file
		)
	}

	// ignore gtk_im_context_simple_add_table deprecated
}