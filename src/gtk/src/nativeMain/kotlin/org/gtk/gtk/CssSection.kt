package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.glib.UnrefMe
import org.gtk.gtk.CssLocation.Companion.wrap

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/struct.CssSection.html">
 *     GtkCssSection</a>
 */
class CssSection(
	val structPointer: CPointer<GtkCssSection>,
) : UnrefMe {

	constructor(file: File?, start: CssLocation, end: CssLocation) :
			this(gtk_css_section_new(
				file?.filePointer,
				start.struct,
				end.struct
			)!!)

	val endLocation: CssLocation
		get() = gtk_css_section_get_end_location(structPointer)!!.wrap()

	val file: File?
		get() = gtk_css_section_get_file(structPointer).wrap()

	val parent: CssSection?
		get() = gtk_css_section_get_parent(structPointer).wrap()

	val startLocation: CssLocation
		get() = gtk_css_section_get_start_location(structPointer)!!.wrap()

	fun ref(): CssSection =
		gtk_css_section_ref(structPointer)!!.wrap()

	override fun toString(): String =
		gtk_css_section_to_string(structPointer)!!.toKString()

	override fun unref() {
		gtk_css_section_unref(structPointer)
	}

	companion object {

		inline fun CPointer<GtkCssSection>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkCssSection>.wrap() =
			CssSection(this)
	}
}