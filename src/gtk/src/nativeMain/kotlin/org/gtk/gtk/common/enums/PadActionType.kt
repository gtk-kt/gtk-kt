package org.gtk.gtk.common.enums

import gtk.GtkPadActionType
import gtk.GtkPadActionType.*

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/enum.PadActionType.html">
 *     GtkPadActionType</a>
 */
enum class PadActionType(
	val gtk: GtkPadActionType,
) {
	BUTTON(GTK_PAD_ACTION_BUTTON),
	RING(GTK_PAD_ACTION_RING),
	STRIP(GTK_PAD_ACTION_STRIP);

	companion object{
		fun valueOf(gtk: GtkPadActionType)=
			when(gtk){
				GTK_PAD_ACTION_BUTTON -> BUTTON
				GTK_PAD_ACTION_RING ->RING
				GTK_PAD_ACTION_STRIP -> STRIP
			}
	}
}