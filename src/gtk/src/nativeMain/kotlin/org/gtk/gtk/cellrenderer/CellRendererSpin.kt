package org.gtk.gtk.cellrenderer

import gtk.GTK_TYPE_CELL_RENDERER_SPIN
import gtk.GtkCellRendererSpin_autoptr
import gtk.gtk_cell_renderer_spin_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget


/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererSpin.html">
 *     GtkCellRendererSpin</a>
 */
class CellRendererSpin(
	val spinPointer: GtkCellRendererSpin_autoptr,
) : CellRendererText(spinPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_spin_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_SPIN
	))
}