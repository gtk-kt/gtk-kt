package org.gtk.gtk.controller.gesture.single

import glib.gdouble
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.AxisUse
import org.gtk.gdk.AxisUse.Companion.toCArray
import org.gtk.gdk.DeviceTool
import org.gtk.gdk.DeviceTool.Companion.wrap
import org.gtk.gdk.TimeCoord
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.controller.gesture.single.GestureLongPress.Companion.wrap

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureStylus.html">
 *     GtkGestureStylus</a>
 */
class GestureStylus(
	val stylusPointer: CPointer<GtkGestureStylus>,
) : GestureSingle(stylusPointer.reinterpret()) {

	constructor() : this(gtk_gesture_stylus_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_LONG_PRESS))

	fun getAxes(axes: Array<AxisUse>): Array<Double>? =
		memScoped {
			val values: CPointer<DoubleVarOf<Double>> =
				allocArray(axes.size)

			val r = gtk_gesture_stylus_get_axes(
				stylusPointer,
				axes.toCArray(),
				values.reinterpret()
			)

			if (r.bool) {
				Array(axes.size) {
					values[it]
				}
			} else null
		}

	fun getAxis(axisUse: AxisUse): Double? =
		memScoped {
			val value = alloc<DoubleVar>()

			val r = gtk_gesture_stylus_get_axis(
				stylusPointer,
				axisUse.gdk,
				value.ptr
			)

			if (r.bool) value.value else null
		}

	val backLog: Array<TimeCoord>?
		get() = memScoped {
			val size = alloc<UIntVar>()
			val backlog = allocPointerTo<GdkTimeCoord>().ptr

			val r = gtk_gesture_stylus_get_backlog(
				stylusPointer,
				backlog,
				size.ptr
			)

			if (r.bool) {
				Array(size.value.toInt()) {
					TimeCoord(backlog[it]!!.pointed)
				}
			} else null
		}

	val deviceTool: DeviceTool?
		get() = gtk_gesture_stylus_get_device_tool(stylusPointer).wrap()

	fun addOnDownCallback(action: GestureStylusFunc) =
		addSignalCallback(Signals.SWIPE, action, staticPressedFunc)

	fun addOnMotionCallback(action: GestureStylusFunc) =
		addSignalCallback(Signals.SWIPE, action, staticPressedFunc)

	fun addOnProximityCallback(action: GestureStylusFunc) =
		addSignalCallback(Signals.SWIPE, action, staticPressedFunc)

	fun addOnUpCallback(action: GestureStylusFunc) =
		addSignalCallback(Signals.SWIPE, action, staticPressedFunc)

	companion object {

		inline fun CPointer<GtkGestureStylus>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkGestureStylus>.wrap() =
			GestureStylus(this)

		private val staticPressedFunc: GCallback =
			staticCFunction {
					self: GtkGestureLongPress_autoptr,
					x: gdouble,
					y: gdouble,
					data: gpointer,
				->
				data.asStableRef<GestureLongPressPressedFunc>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()
	}
}

typealias GestureStylusFunc =
		GestureStylus.(
			x: Double,
			y: Double,
		) -> Unit