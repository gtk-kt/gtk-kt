package org.gtk.gtk

import gtk.GtkConstraintGuide_autoptr
import gtk.gtk_constraint_guide_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * kotlinx-gtk
 *
 * 30 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/ctor.ConstraintGuide.new.html">
 *     GtkConstraintGuide</a>
 */
class ConstraintGuide(
	val guidePointer: GtkConstraintGuide_autoptr
) : KGObject(guidePointer.reinterpret()) {

	constructor() : this(gtk_constraint_guide_new()!!)

	companion object {
		inline fun GtkConstraintGuide_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkConstraintGuide_autoptr.wrap() =
			ConstraintGuide(this)
	}
}