package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gobject.TypedNoArgFunc

/**
 * 14 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EventControllerMotion.html">
 *     GtkEventControllerMotion</a>
 */
class EventControllerMotion(
	val motionPointer: CPointer<GtkEventControllerMotion>,
) : EventController(motionPointer.reinterpret()) {

	constructor() : this(gtk_event_controller_motion_new()!!.reinterpret())

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_EVENT_CONTROLLER_MOTION
	))

	val containsPointer: Boolean
		get() = gtk_event_controller_motion_contains_pointer(motionPointer).bool

	val isPointer: Boolean
		get() = gtk_event_controller_motion_is_pointer(motionPointer).bool

	fun addOnEnterCallback(action: EventControllerMotionXYFunc) =
		addSignalCallback(Signals.ENTER, action, staticXYGCallback)

	fun addOnLeaveCallback(action: TypedNoArgFunc<EventControllerMotion>) =
		addSignalCallback(Signals.LEAVE, action, staticNoArgGCallback)

	fun addOnMotionCallback(action: EventControllerMotionXYFunc) =
		addSignalCallback(Signals.MOTION, action, staticXYGCallback)

	companion object {
		inline fun CPointer<GtkEventControllerMotion>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkEventControllerMotion>.wrap() =
			EventControllerMotion(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerMotion>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<EventControllerMotion>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticXYGCallback: GCallback =
			staticCFunction {
					self: CPointer<GtkEventControllerMotion>,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<EventControllerMotionXYFunc>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()
	}
}

typealias EventControllerMotionXYFunc =
		EventControllerMotion.(
			x: Double,
			y: Double,
		) -> Unit