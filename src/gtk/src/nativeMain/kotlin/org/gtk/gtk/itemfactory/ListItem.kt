package org.gtk.gtk.itemfactory

import gobject.GObject
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.ListItem.html">
 *     GtkListItem</a>
 */
class ListItem(val listItemPointer: GtkListItem_autoptr) :
	KGObject(listItemPointer.reinterpret()) {

	var activatable: Boolean
		get() = gtk_list_item_get_activatable(listItemPointer).bool
		set(value) = gtk_list_item_set_activatable(listItemPointer, value.gtk)

	var child: Widget?
		get() = gtk_list_item_get_child(listItemPointer).wrap()
		set(value) = gtk_list_item_set_child(
			listItemPointer,
			value?.widgetPointer
		)

	val item: KGObject?
		get() = gtk_list_item_get_item(listItemPointer)
			?.reinterpret<GObject>().wrap()

	val position: UInt
		get() = gtk_list_item_get_position(listItemPointer)

	var selectable: Boolean
		get() = gtk_list_item_get_selectable(listItemPointer).bool
		set(value) = gtk_list_item_set_selectable(listItemPointer, value.gtk)

	companion object {
		inline fun GtkListItem_autoptr.wrap() = ListItem(this)
		inline fun GtkListItem_autoptr?.wrap() = this?.wrap()
	}
}