package org.gtk.gtk.controller.gesture.single

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.ContentProvider
import org.gtk.gdk.ContentProvider.Companion.wrap
import org.gtk.gdk.Drag
import org.gtk.gdk.Drag.Companion.wrap
import org.gtk.gdk.DragCancelReason
import org.gtk.gdk.Paintable
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DragSource.html">
 *     GtkDragSource</a>
 */
class DragSource(
	val dragPointer: CPointer<GtkDragSource>,
) : GestureSingle(dragPointer.reinterpret()) {

	constructor() : this(gtk_drag_source_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_DRAG_SOURCE))

	fun cancelDrag() {
		gtk_drag_source_drag_cancel(dragPointer)
	}

	var actions: GdkDragAction
		get() = gtk_drag_source_get_actions(dragPointer)
		set(value) = gtk_drag_source_set_actions(dragPointer, value)

	var content: ContentProvider?
		get() = gtk_drag_source_get_content(dragPointer).wrap()
		set(value) = gtk_drag_source_set_content(
			dragPointer,
			value?.contentProviderPointer
		)

	val drag: Drag?
		get() = gtk_drag_source_get_drag(dragPointer).wrap()

	fun setIcon(paintable: Paintable?, hotX: Int, hotY: Int) {
		gtk_drag_source_set_icon(
			dragPointer,
			paintable?.paintablePointer,
			hotX,
			hotY
		)
	}

	fun addOnDragBeginCallback(action: DragSourceBeginFunc) =
		addSignalCallback(Signals.DRAG_BEGIN, action, staticBeginFunc)

	fun addOnDragCancelCallback(action: DragSourceCancelFunc) =
		addSignalCallback(Signals.DRAG_CANCEL, action, staticCancelFunc)

	fun addOnDragEndCallback(action: DragSourceEndFunc) =
		addSignalCallback(Signals.DRAG_END, action, staticEndFunc)

	fun addOnDragPrepareCallback(action: DragSourcePrepareFunc) =
		addSignalCallback(Signals.PREPARE, action, staticPrepareFunc)


	companion object {

		inline fun CPointer<GtkDragSource>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkDragSource>.wrap() =
			DragSource(this)

		private val staticBeginFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDragSource>,
					drag: GdkDrag_autoptr,
					data: gpointer,
				->
				data.asStableRef<DragSourceBeginFunc>()
					.get()
					.invoke(self.wrap(), drag.wrap())
			}.reinterpret()

		private val staticCancelFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDragSource>,
					drag: GdkDrag_autoptr,
					reason: GdkDragCancelReason,
					data: gpointer,
				->
				data.asStableRef<DragSourceCancelFunc>()
					.get()
					.invoke(
						self.wrap(),
						drag.wrap(),
						DragCancelReason.valueOf(reason)
					).gtk
			}.reinterpret()

		private val staticEndFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDragSource>,
					drag: GdkDrag_autoptr,
					reason: gboolean,
					data: gpointer,
				->
				data.asStableRef<DragSourceEndFunc>()
					.get()
					.invoke(
						self.wrap(),
						drag.wrap(),
						reason.bool
					)
			}.reinterpret()


		private val staticPrepareFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDragSource>,
					velocity_x: Double,
					velocity_y: Double,
					data: gpointer,
				->
				data.asStableRef<DragSourcePrepareFunc>()
					.get()
					.invoke(self.wrap(), velocity_x, velocity_y)
					.contentProviderPointer
			}.reinterpret()
	}
}

typealias DragSourceBeginFunc =
		DragSource.(
			drag: Drag,
		) -> Unit

typealias DragSourceCancelFunc =
		DragSource.(
			drag: Drag,
			reason: DragCancelReason,
		) -> Boolean

typealias DragSourceEndFunc =
		DragSource.(
			drag: Drag,
			deleteData: Boolean,
		) -> Unit

typealias DragSourcePrepareFunc =
		DragSource.(
			x: Double,
			y: Double,
		) -> ContentProvider