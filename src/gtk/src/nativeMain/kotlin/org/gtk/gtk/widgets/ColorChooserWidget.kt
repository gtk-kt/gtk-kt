package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.ColorChooser

/**
 * 19 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColorChooserWidget.html">
 *     GtkColorChooserWidget</a>
 */
class ColorChooserWidget(
	val colorChooserWidgetPointer: GtkColorChooserWidget_autoptr,
) : Widget(colorChooserWidgetPointer.reinterpret()), ColorChooser {
	override val colorChooserPointer: CPointer<GtkColorChooser> by lazy {
		colorChooserWidgetPointer.reinterpret()
	}

	constructor() : this(gtk_color_chooser_widget_new()!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_COLOR_CHOOSER_WIDGET))

}