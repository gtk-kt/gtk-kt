package org.gtk.gtk.common.enums;

import gtk.GtkSensitivityType
import gtk.GtkSensitivityType.*

enum class SensitivityType(val gtk: GtkSensitivityType) {
	AUTO(GTK_SENSITIVITY_AUTO),
	ON(GTK_SENSITIVITY_ON),
	OFF(GTK_SENSITIVITY_OFF);

	companion object {
		fun valueOf(gtk: GtkSensitivityType) =
			when (gtk) {
				GTK_SENSITIVITY_AUTO -> AUTO
				GTK_SENSITIVITY_ON -> ON
				GTK_SENSITIVITY_OFF -> OFF
			}
	}
}