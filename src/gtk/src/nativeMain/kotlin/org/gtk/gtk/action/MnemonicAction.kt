package org.gtk.gtk.action

import gtk.GTK_TYPE_MNEMONIC_ACTION
import gtk.GtkMnemonicAction_autoptr
import gtk.gtk_mnemonic_action_get
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MnemonicAction.html">
 *     GtkMnemonicAction</a>
 */
class MnemonicAction(val mnemonicAction: GtkMnemonicAction_autoptr) :
	ShortcutAction(mnemonicAction.reinterpret()) {

	constructor() : this(gtk_mnemonic_action_get()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_MNEMONIC_ACTION))

	companion object {
		inline fun GtkMnemonicAction_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkMnemonicAction_autoptr.wrap() =
			MnemonicAction(this)
	}
}