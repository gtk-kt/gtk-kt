package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 *
 * 24 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.HeaderBar.html">GtkHeaderBar</a>
 */
open class HeaderBar(
	val headerBarPointer: CPointer<GtkHeaderBar>,
) : Widget(headerBarPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_HEADER_BAR))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.HeaderBar.new.html">
	 *     gtk_header_bar_new</a>
	 */
	constructor() : this(gtk_header_bar_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.HeaderBar.pack_start.html">
	 *     gtk_header_bar_pack_start</a>
	 */
	fun packStart(child: Widget) {
		gtk_header_bar_pack_start(headerBarPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.HeaderBar.pack_end.html">
	 *     gtk_header_bar_pack_end</a>
	 */
	fun packEnd(child: Widget) {
		gtk_header_bar_pack_end(headerBarPointer, child.widgetPointer)
	}

	fun remove(child: Widget) {
		gtk_header_bar_remove(headerBarPointer, child.widgetPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.HeaderBar.get_decoration_layout.html">
	 *     gtk_header_bar_get_decoration_layout</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.HeaderBar.set_decoration_layout.html">
	 *     gtk_header_bar_set_decoration_layout</a>
	 */
	var decorationLayout: String?
		get() =
			gtk_header_bar_get_decoration_layout(headerBarPointer)?.toKString()
		set(value) = gtk_header_bar_set_decoration_layout(
			headerBarPointer,
			value
		)

	var showTitleButtons: Boolean
		get() = gtk_header_bar_get_show_title_buttons(headerBarPointer).bool
		set(value) = gtk_header_bar_set_show_title_buttons(
			headerBarPointer,
			value.gtk
		)

	var titleWidget: Widget?
		get() = gtk_header_bar_get_title_widget(headerBarPointer).wrap()
		set(value) = gtk_header_bar_set_title_widget(
			headerBarPointer,
			value?.widgetPointer
		)

	companion object {
		inline fun CPointer<GtkHeaderBar>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkHeaderBar>.wrap() =
			HeaderBar(this)
	}


}