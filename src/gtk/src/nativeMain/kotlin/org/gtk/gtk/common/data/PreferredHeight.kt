package org.gtk.gtk.common.data

data class PreferredHeight(
	val minimumHeight: Int,
	val naturalHeight: Int,
)