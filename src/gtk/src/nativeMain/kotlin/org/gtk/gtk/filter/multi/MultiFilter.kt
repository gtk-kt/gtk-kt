package org.gtk.gtk.filter.multi

import gtk.GtkMultiFilter_autoptr
import gtk.gtk_multi_filter_append
import gtk.gtk_multi_filter_remove
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.filter.Filter

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MultiFilter.html">
 *     GtkMultiFilter</a>
 */
open class MultiFilter(val multiFilterPointer: GtkMultiFilter_autoptr) :
	Filter(multiFilterPointer.reinterpret()) {

	fun append(filter: Filter) {
		gtk_multi_filter_append(multiFilterPointer, filter.filterPointer)
	}

	fun remove(position: UInt) {
		gtk_multi_filter_remove(multiFilterPointer, position)
	}
}