package org.gtk.gtk.widgets.entry

import gtk.GTK_TYPE_SEARCH_ENTRY
import gtk.GtkEditable
import gtk.GtkSearchEntry
import gtk.gtk_search_entry_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Editable
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 16 / 03 / 2021
 *
 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html">GtkSearchEntry</a>
 */
class SearchEntry(val searchEntryPointer: CPointer<GtkSearchEntry>) :
	Entry(searchEntryPointer.reinterpret()),
	Editable {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SEARCH_ENTRY))

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html#gtk-search-entry-new">
	 *     gtk_search_entry_new</a>
	 */
	constructor() : this(gtk_search_entry_new()!!.reinterpret())

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html#GtkSearchEntry-next-match">
	 *     next-match</a>
	 */
	fun addOnNextMatchCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
			Signals.NEXT_MATCH,
			action
		)

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html#GtkSearchEntry-previous-match">
	 *     previous-match</a>
	 */
	fun addOnPreviousMatchCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
			Signals.PREVIOUS_MATCH,
			action
		)

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html#GtkSearchEntry-search-changed">
	 *     search-changed</a>
	 */
	fun addOnSearchChangedCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
			Signals.SEARCH_CHANGED,
			action
		)

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html#GtkSearchEntry-stop-search">
	 *     stop-search</a>
	 */
	fun addOnStopSearchCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
			Signals.STOP_SEARCH,
			action
		)

	override val editablePointer: CPointer<GtkEditable> by lazy { searchEntryPointer.reinterpret() }
}