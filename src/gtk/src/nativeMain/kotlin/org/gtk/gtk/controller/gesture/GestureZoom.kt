package org.gtk.gtk.controller.gesture

import gtk.GTK_TYPE_GESTURE_ZOOM
import gtk.GtkGestureZoom_autoptr
import gtk.gtk_gesture_zoom_get_scale_delta
import gtk.gtk_gesture_zoom_new
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureZoom.html">
 *     GtkGestureZoom</a>
 */
class GestureZoom(
	val zoomPointer: GtkGestureZoom_autoptr,
) : Gesture(zoomPointer.reinterpret()) {

	constructor() : this(gtk_gesture_zoom_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_ZOOM))

	val scaleDelta: Double
		get() = gtk_gesture_zoom_get_scale_delta(zoomPointer)
}