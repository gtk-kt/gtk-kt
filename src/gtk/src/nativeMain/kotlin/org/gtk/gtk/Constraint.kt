package org.gtk.gtk

import gtk.GtkConstraint_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * kotlinx-gtk
 *
 * 23 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Constraint.html">
 *     GtkConstraint</a>
 */
class Constraint(
	val constraintPointer: GtkConstraint_autoptr
) : KGObject(constraintPointer.reinterpret()) {

}