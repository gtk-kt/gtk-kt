package org.gtk.gtk.widgets.windows.dialog

import gtk.*
import gtk.GtkButtonsType.*
import gtk.GtkMessageType.*
import kotlinx.cinterop.cstr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.MessageDialog.html">
 *     GtkMessageDialog</a>
 */
class MessageDialog(
	@Suppress("MemberVisibilityCanBePrivate")
	val messageDialogPointer: GtkMessageDialog_autoptr,
) : Dialog(messageDialogPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_MESSAGE_DIALOG))

	/**
	 * TODO vararg management
	 */
	constructor(
		parent: Window? = null,
		flags: Flags,
		messageType: MessageType,
		buttonsType: ButtonsType,
		message: String,
		withMarkup: Boolean = false,
	) : this(
		if (!withMarkup)
			gtk_message_dialog_new(
				parent?.windowPointer,
				flags.gtk,
				messageType.gtk,
				buttonsType.gtk,
				"%s",
				message.cstr
			)!!.reinterpret()
		else
			gtk_message_dialog_new_with_markup(
				parent?.windowPointer,
				flags.gtk,
				messageType.gtk,
				buttonsType.gtk,
				"%s",
				message.cstr
			)!!.reinterpret()
	)

	fun setMarkup(markup: String) {
		gtk_message_dialog_set_markup(messageDialogPointer, markup)
	}

	fun formatSecondaryText(
		messageFormat: String? = null,
	) {
		gtk_message_dialog_format_secondary_text(
			messageDialogPointer,
			messageFormat,
		)
	}

	fun formatSecondaryMarkup(
		messageFormat: String? = null,
	) {
		gtk_message_dialog_format_secondary_markup(
			messageDialogPointer,
			messageFormat,
		)
	}

	val messageArea: Widget?
		get() = gtk_message_dialog_get_message_area(messageDialogPointer)?.let {
			Widget(
				it
			)
		}

	enum class MessageType(val gtk: GtkMessageType) {
		INFO(GTK_MESSAGE_INFO),
		WARNING(GTK_MESSAGE_WARNING),
		QUESTION(GTK_MESSAGE_QUESTION),
		ERROR(GTK_MESSAGE_ERROR),
		OTHER(GTK_MESSAGE_OTHER);

		companion object {
			fun valueOf(gtk: GtkMessageType) =
				when (gtk) {
					GTK_MESSAGE_INFO -> INFO
					GTK_MESSAGE_WARNING -> WARNING
					GTK_MESSAGE_QUESTION -> QUESTION
					GTK_MESSAGE_ERROR -> ERROR
					GTK_MESSAGE_OTHER -> OTHER
				}
		}
	}

	enum class ButtonsType(val gtk: GtkButtonsType) {
		NONE(GTK_BUTTONS_NONE),
		OK(GTK_BUTTONS_OK),
		CLOSE(GTK_BUTTONS_CLOSE),
		CANCEL(GTK_BUTTONS_CANCEL),
		YES_NO(GTK_BUTTONS_YES_NO),
		OK_CANCEL(GTK_BUTTONS_OK_CANCEL);

		companion object {
			fun valueOf(gtk: GtkButtonsType) =
				when (gtk) {
					GTK_BUTTONS_NONE -> NONE
					GTK_BUTTONS_OK -> OK
					GTK_BUTTONS_CLOSE -> CLOSE
					GTK_BUTTONS_CANCEL -> CANCEL
					GTK_BUTTONS_YES_NO -> YES_NO
					GTK_BUTTONS_OK_CANCEL -> OK_CANCEL
				}
		}
	}
}