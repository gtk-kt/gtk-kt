package org.gtk.gtk.cellrenderer

import glib.gpointer
import gobject.GCallback
import gtk.GTK_TYPE_CELL_RENDERER_TEXT
import gtk.GtkCellRendererText_autoptr
import gtk.gtk_cell_renderer_text_new
import gtk.gtk_cell_renderer_text_set_fixed_height_from_font
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererText.html">
 *     GtkCellRendererText</a>
 */
open class CellRendererText(
	val textPointer: GtkCellRendererText_autoptr,
) : CellRenderer(textPointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_text_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_TEXT
	))

	fun setFixedHeightFromFont(numberOfRows: Int) {
		gtk_cell_renderer_text_set_fixed_height_from_font(
			textPointer,
			numberOfRows
		)
	}

	fun addOnEditedCallback(action: CellRendererTextEditedFunc) =
		addSignalCallback(Signals.EDITED, action, staticEditedCallback)

	companion object {
		inline fun GtkCellRendererText_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellRendererText_autoptr.wrap() =
			CellRendererText(this)

		private val staticEditedCallback: GCallback =
			staticCFunction {
					self: GtkCellRendererText_autoptr,
					path: CStringPointer,
					new_text: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellRendererTextEditedFunc>()
					.get()
					.invoke(
						self.wrap(),
						path.toKString(),
						new_text.toKString()
					)
			}.reinterpret()
	}
}

typealias CellRendererTextEditedFunc =
		CellRendererText.(
			path: String,
			newText: String,
		) -> Unit