package org.gtk.gtk.controller

import glib.gpointer
import gobject.GCallback
import gobject.GType
import gobject.GValue
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.ContentFormats
import org.gtk.gdk.ContentFormats.Companion.wrap
import org.gtk.gdk.Drop
import org.gtk.gdk.Drop.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.glib.toCArray
import org.gtk.gobject.*
import org.gtk.gobject.Value.Companion.wrap
import org.gtk.gobject.TypedNoArgFunc

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DropTarget.html">
 *     GtkDropTarget</a>
 */
class DropTarget(
	val dropTargetPointer: CPointer<GtkDropTarget>,
) : EventController(dropTargetPointer.reinterpret()) {

	constructor(type: KGType, actions: GdkDragAction) :
			this(gtk_drop_target_new(type.glib, actions)!!)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_DROP_TARGET
	))

	var actions: GdkDragAction
		get() = gtk_drop_target_get_actions(dropTargetPointer)
		set(value) = gtk_drop_target_set_actions(dropTargetPointer, value)

	val currentDrop: Drop?
		get() = gtk_drop_target_get_current_drop(dropTargetPointer).wrap()

	val drop: Drop?
		get() = gtk_drop_target_get_drop(dropTargetPointer).wrap()

	val formats: ContentFormats?
		get() = gtk_drop_target_get_formats(dropTargetPointer).wrap()

	var gtypes: Array<GType>?
		get() = memScoped {
			val length = alloc<ULongVar>()

			val array = gtk_drop_target_get_gtypes(
				dropTargetPointer,
				length.ptr
			)

			if (array != null)
				Array(length.value.toInt()) { array[it] }
			else null
		}
		set(value) = memScoped {
			gtk_drop_target_set_gtypes(
				dropTargetPointer,
				value?.toCArray(this),
				value?.size?.toULong() ?: 0uL
			)
		}

	var preload: Boolean
		get() = gtk_drop_target_get_preload(dropTargetPointer).bool
		set(value) = gtk_drop_target_set_preload(dropTargetPointer, value.gtk)

	val value: Value?
		get() = gtk_drop_target_get_value(dropTargetPointer).wrap()

	fun reject() {
		gtk_drop_target_reject(dropTargetPointer)
	}

	fun addOnAcceptCallback(action: DropTargetAcceptFunc) =
		addSignalCallback(Signals.ACCEPT, action, staticAcceptFunc)

	fun addOnDropCallback(action: DropTargetDropFunc) =
		addSignalCallback(Signals.DROP, action, staticDropFunc)

	fun addOnEnterCallback(action: DropTargetXYForDragFunc) =
		addSignalCallback(Signals.ENTER, action, staticXYFunc)

	fun addOnLeaveCallback(action: TypedNoArgFunc<DropTarget>) =
		addSignalCallback(Signals.LEAVE, action, staticNoArgFunc)

	fun addOnMotionCallback(action: DropTargetXYForDragFunc) =
		addSignalCallback(Signals.MOTION, action, staticXYFunc)

	companion object {
		inline fun CPointer<GtkDropTarget>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkDropTarget>.wrap() =
			DropTarget(this)

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTarget>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<DropTarget>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticAcceptFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTarget>,
					drop: GdkDrop_autoptr,
					data: gpointer,
				->
				data.asStableRef<DropTargetAcceptFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap())
			}.reinterpret()

		private val staticDropFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTarget>,
					drop: CPointer<GValue>,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<DropTargetDropFunc>()
					.get()
					.invoke(self.wrap(), drop.wrap(), x, y)
					.gtk
			}.reinterpret()


		private val staticXYFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkDropTarget>,
					x: Double,
					y: Double,
					data: gpointer,
				->
				data.asStableRef<DropTargetXYForDragFunc>()
					.get()
					.invoke(self.wrap(), x, y)
			}.reinterpret()
	}
}

typealias DropTargetAcceptFunc =
		DropTarget.(Drop) -> Boolean

typealias DropTargetDropFunc =
		DropTarget.(Value, x: Double, y: Double) -> Boolean

typealias DropTargetXYForDragFunc =
		DropTarget.(x: Double, y: Double) -> GdkDragAction




