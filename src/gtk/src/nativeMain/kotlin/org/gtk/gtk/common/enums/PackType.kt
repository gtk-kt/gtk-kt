package org.gtk.gtk.common.enums

import gtk.GtkPackType
import gtk.GtkPackType.GTK_PACK_END
import gtk.GtkPackType.GTK_PACK_START

/**
 * kotlinx-gtk
 * 06 / 03 / 2021
 */
enum class PackType(val gtk: GtkPackType) {
	START(GTK_PACK_START),
	END(GTK_PACK_END);

	companion object {
		fun valueOf(gtk: GtkPackType): PackType =
			when (gtk) {
				GTK_PACK_START -> START
				GTK_PACK_END -> END
			}
	}
}