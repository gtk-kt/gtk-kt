package org.gtk.gdk

import gtk.GdkDrop_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.Drop.html">GdkDrop</a>
 */
class Drop(val dropPointer: GdkDrop_autoptr) :
	KGObject(dropPointer.reinterpret()) {

	companion object {
		inline fun GdkDrop_autoptr?.wrap() =
			this?.wrap()

		inline fun GdkDrop_autoptr.wrap() =
			Drop(this)
	}
}