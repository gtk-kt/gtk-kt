package org.gtk.gdk

import gtk.GdkAxisUse
import gtk.GdkAxisUse.*
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gdk4/enum.AxisUse.html">
 *     GdkAxisUse</a>
 */
enum class AxisUse(
	val gdk: GdkAxisUse,
) {
	IGNORE(GDK_AXIS_IGNORE),
	X(GDK_AXIS_X),
	Y(GDK_AXIS_Y),
	DELTA_X(GDK_AXIS_DELTA_X),
	DELTA_Y(GDK_AXIS_DELTA_Y),
	PRESSURE(GDK_AXIS_PRESSURE),
	XTILT(GDK_AXIS_XTILT),
	YTILT(GDK_AXIS_YTILT),
	WHEEL(GDK_AXIS_WHEEL),
	DISTANCE(GDK_AXIS_DISTANCE),
	ROTATION(GDK_AXIS_ROTATION),
	SLIDER(GDK_AXIS_SLIDER),
	LAST(GDK_AXIS_LAST);

	companion object {
		fun valueOf(gdk: GdkAxisUse): AxisUse =
			when (gdk) {
				GDK_AXIS_IGNORE -> IGNORE
				GDK_AXIS_X -> X
				GDK_AXIS_Y -> Y
				GDK_AXIS_DELTA_X -> DELTA_X
				GDK_AXIS_DELTA_Y -> DELTA_Y
				GDK_AXIS_PRESSURE -> PRESSURE
				GDK_AXIS_XTILT -> XTILT
				GDK_AXIS_YTILT -> YTILT
				GDK_AXIS_WHEEL -> WHEEL
				GDK_AXIS_DISTANCE -> DISTANCE
				GDK_AXIS_ROTATION -> ROTATION
				GDK_AXIS_SLIDER -> SLIDER
				GDK_AXIS_LAST -> LAST
			}

		inline fun Array<AxisUse>.toCArray() = memScoped {
			allocArray<Var>(this@toCArray.size) {
				value = this@toCArray[it].gdk
			}
		}
	}
}