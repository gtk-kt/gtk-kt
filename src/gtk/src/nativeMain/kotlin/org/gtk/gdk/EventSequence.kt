package org.gtk.gdk

import gtk.GdkEventSequence
import kotlinx.cinterop.CPointer

/**
 * gtk-kt
 *
 * 26 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/struct.EventSequence.html">
 *     GdkEventSequence</a>
 */
class EventSequence(val eventSequencePointer: CPointer<GdkEventSequence>){



	companion object{
		inline fun CPointer<GdkEventSequence>?.wrap() =
			this?.wrap()

		inline fun CPointer<GdkEventSequence>.wrap() =
			EventSequence(this)
	}
}