plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "1.0.0-alpha1"
description = "Kotlin bindings for GTK4, targets 4.4.1"

kotlin {
	native {
		val main by compilations.getting
		val gtk by main.cinterops.creating
		//val gtkunixprint by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				api(project(":src:gio"))
				api(project(":src:cairo"))
				api(project(":src:pango"))
				api(project(":src:gobject"))
				api(project(":src:glib"))
				api(project(":src:gdk-pixbuf"))
			}
		}
	}
}

apply(plugin = "native-publish")