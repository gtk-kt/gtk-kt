package org.gtk.pango

import kotlinx.cinterop.toKString
import pango.pango_version_string

/**
 * gtk-kt
 *
 * 24 / 09 / 2021
 *
 * @see <a href=""></a>
 */
val pangoVersionString
	get() = pango_version_string()!!.toKString()