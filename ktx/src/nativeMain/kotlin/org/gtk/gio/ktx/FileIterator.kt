package org.gtk.gio.ktx

import org.gtk.gio.FileEnumerator
import org.gtk.gio.FileInfo

class FileIterator(val fileEnumerator: FileEnumerator) : Iterator<FileInfo>, Iterable<FileInfo> {

	private var last: FileInfo? = null

	override fun hasNext(): Boolean {
		return fileEnumerator.nextFile().also { last = it } != null
	}

	override fun next(): FileInfo {
		return last ?: throw NullPointerException("no fileInfo was returned")
	}

	companion object {
		inline operator fun FileEnumerator.iterator() =
			FileIterator(this)
	}

	override fun iterator(): Iterator<FileInfo> = this
}