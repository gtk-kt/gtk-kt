package org.gtk.glib.ktx

import org.gtk.glib.Variant
import org.gtk.glib.VariantDictionary

/*
 * gtk-kt
 *
 * 16 / 10 / 2021
 */

inline operator fun VariantDictionary.set(key: String, value: Variant) =
	insert(key, value)

inline operator fun VariantDictionary.get(key: String) =
	lookup(key, null)
