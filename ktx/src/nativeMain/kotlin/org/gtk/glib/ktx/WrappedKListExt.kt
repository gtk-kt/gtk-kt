package org.gtk.glib.ktx

import kotlinx.cinterop.*
import org.gtk.glib.KList
import org.gtk.glib.WrappedKList

/*
 * kotlinx-gtk
 *
 * 21 / 08 / 2021
 */

/**
 * Easy way to define a string list
 */
fun WrappedStringKList(memScope: AutofreeScope, kList: KList = KList()) =
	WrappedKList(
		kList = kList,
		wrapPointer = { reinterpret<ByteVarOf<Byte>>().toKString() },
		{ cstr.getPointer(memScope) }
	)