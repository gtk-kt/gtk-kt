
import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration

plugins {
	kotlin("multiplatform")
	id("org.jetbrains.dokka")
}

allprojects {
	group = "org.gtk-kt"

	repositories {
		mavenCentral()
	}
}



kotlin {
	mingwX64()
	macosX64()
	linuxX64()
}

tasks {
	dokkaHtmlMultiModule {
		outputDirectory.set(buildDir.resolve("dokka"))

		includes.from("dokka/includes/home.md")

		pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
			customStyleSheets =
				rootDir
					.resolve("dokka/stylesheets")
					.listFiles()
					.toList()

			customAssets =
				rootDir
					.resolve("dokka/assets")
					.walkTopDown()
					.filter { it.isFile }
					.toList()
		}
	}
}
