package org.gtk.dsl.gtk

import gio.GApplicationFlags
import gio.G_APPLICATION_FLAGS_NONE
import org.gtk.dsl.GtkDsl
import org.gtk.gtk.Application

/**
 * kotlinx-gtk
 * 07 / 03 / 2021
 */

@GtkDsl
inline fun application(
	id: String,
	flags: GApplicationFlags = G_APPLICATION_FLAGS_NONE,
	crossinline builder: Application.() -> Unit,
): Int {
	val app = Application(id, flags).apply(builder)
	val exitStatus = app.run()
	app.unref()
	return exitStatus
}