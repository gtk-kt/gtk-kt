package org.gtk.dsl.glib

import org.gtk.gobject.KGType
import org.gtk.gobject.Value

fun kGValue(value: Int) = Value(KGType.INT).apply { int = value }

fun kGValue(value: String) = Value(KGType.STRING).apply { string = value }